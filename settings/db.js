// Conexion.
var mysql = require('mysql'); // Invocacion de mysql.
// require('dotenv').config({ path: './.env' })
const { database } = require('./dbSetting'); // Invocacion de configuración de la conexion.

// Estableciendo conexion.
var conn = mysql.createPool({
  // host: process.env.DB_HOST,
  // port: process.env.DB_PORT,
  // user: process.env.DB_USER,
  // password: process.env.DB_PASS,
  // database: process.env.DB_DB
  host: database.DB_HOST,
  port: database.DB_PORT,
  user: database.DB_USER,
  password: database.DB_PASS,
  database: database.DB_DB
});

// Verificación de conexión estable.
conn.getConnection((err, connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // console.error(err.stack);
      console.error('Se cerró la conexión a la base de datos.');
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      // console.error(err.stack);
      console.error('Demaciadas conexiones en la Base de Datos.');
    }
    if (err.code === 'ECONNREFUSED') {
      console.error(err.stack);
      console.error('No se pudo conectar a la Base de Datos.');
    }
    if (err.code === 'ER_ACCESS_DENIED_ERROR') {
      console.error(err.stack);
      console.error('No se pudo conectar a la Base de Datos.');
    }
    console.log(err)
  }
  if (connection) connection.release(); 
  // console.error('connected as id ' + connection.threadId);
  return
});

// Exportamos las funciones (modulos).
module.exports = {conn};  