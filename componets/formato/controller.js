const store = require('./store');

function listarFormato(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarFormato());
    })
}
function agregarFormato(formato){
    return new Promise((resolve, reject)=>{
        if(!formato.plantillaFormato || !formato.nombreformato){
            console.error('[Controller formato] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullFormato = {
            plantilla : formato.plantillaFormato,
            nombre : formato.nombreformato
        }
        resolve(store.agregarFormato(fullFormato));
    })
}
function actualizarFormato(id, formato){
    return new Promise((resolve, reject)=>{
        if(!id || !formato.plantillaFormato || !formato.nombreFormato){
            console.error('[Controller formato]');
            reject('');
            return false;
        }
        const fullFormato = { 
            id, 
            plantilla : formato.plantillaFormato,
            nombre : formato.nombreFormato
        }
        resolve(store.actualizarFormato(fullFormato))
    })
}
function eliminarFormato(id){
    return new Promise((resolve, reject)=>{
        if (!id) {
            console.error('[Controller formato] el identificador del formato es vacio');
            reject('Verificar campo llego vacio');
            return false;
        }
        resolve(store.eliminarFormato(id));
    })
}

module.exports = {
    listarFormato,
    agregarFormato,
    actualizarFormato,
    eliminarFormato
}