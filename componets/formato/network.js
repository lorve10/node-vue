const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarFormato().then((allFormato)=>{
        response.success(req, res, allFormato, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar a los formatos', 500, 'La consulta a los formatos salio mal');
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarFormato(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, '', 400, '');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarFormato(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, '', 400, '');
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarFormato(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, '', 400, '');
    })
})

module.exports=router;