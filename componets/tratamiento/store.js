const {conn} = require('../../settings/db')

function listarTratamiento(){
    return new Promise ((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_tratamiento AS Uid, tipo_tratamiento AS tipo, nombre_tratamiento AS nombre FROM tratamientos',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarTratamiento(tratamiento){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO tratamientos (tipo_tratamiento, nombre_tratamiento) VALUES (?, ?)', [tratamiento.tipo, tratamiento.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarTratamiento(tratamiento){
    console.log(tratamiento);
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error) {throw error}
                connection.query('UPDATE tratamientos SET tipo_tratamiento = ?, nombre_tratamiento = ? WHERE id_tratamiento = ?', [tratamiento.tipo, tratamiento.nombre, tratamiento.id,], (error, results, fields)=>{
                    connection.release();
                    if (error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito');
                        }
                        else{
                            console.error('[]');
                            reject('');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarTratamiento(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM tratamientos WHERE id_tratamiento = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito')
                        }
                        else{
                            console.error('[store Tratamiento] Algo ocurrio al eliminar el tratamiento');
                            reject('Fallo la eliminacion del tratamiento');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarTratamiento,
    agregarTratamiento,
    actualizarTratamiento,
    eliminarTratamiento
}