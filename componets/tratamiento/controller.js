const store = require('./store');

function listarTratamiento(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarTratamiento());
    })
}
function agregarTratamiento(tratamiento){
    return new Promise((resolve, reject)=>{
        if(!tratamiento.tipoTratamiento || !tratamiento.nombre){
            console.error('[Controller Tratamiento] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullTratamiento = {
            tipo : tratamiento.tipoTratamiento,
            nombre : tratamiento.nombre
        }
        resolve(store.agregarTratamiento(fullTratamiento));
    })
}
function actualizarTratamiento(id, tratamiento){
    return new Promise((resolve, reject)=>{
        if (!id || !tratamiento.tipoTratamiento || !tratamiento.nombre) {
            console.error('[Controller Tratamiento] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullTratamiento = {
            id, 
            tipo : tratamiento.tipoTratamiento,
            nombre : tratamiento.nombre
        }
        resolve(store.actualizarTratamiento(fullTratamiento))
    })
}
function eliminarTratamiento(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Tratamiento] la informacion no llego');
            reject('Error al inrtentar eiminar');
            return false;
        }
        resolve (store.eliminarTratamiento(id));
    })
}

module.exports = {
    listarTratamiento,
    agregarTratamiento,
    actualizarTratamiento,
    eliminarTratamiento
}