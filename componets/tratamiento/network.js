const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarTratamiento().then((allTratamiento)=>{
        response.success(req, res, allTratamiento, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar a los tratamiento', 500, 'La consulta a los tratamiento salio mal');
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarTratamiento(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al agregar el tratamiento', 400, 'Error al agregar el tratamiento');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarTratamiento(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al intentar actualizar el tratamiento', 400, 'Error al actualizar el tratamiento');
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarTratamiento(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo fallo al eliminar el tratamiento', 400, 'Error al eliminar el tratamiento');
    })
})

module.exports=router;