const {conn} = require('../../settings/db'); 

function listarTipoItem(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                
                connection.query('SELECT id_tipoItem AS Uid, alias_tipoitem AS alias, nombre_tipoItem AS nombre FROM tipos_items', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarTipoItem(tipoItem){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('INSERT INTO tipos_items (alias_tipoItem, nombre_tipoItem) VALUES (?, ?)',[tipoItem.alias, tipoItem.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarTipoItem(tipoItem){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE tipos_items SET alias_tipoItem = ?, nombre_tipoItem = ? WHERE id_tipoItem = ? ', [tipoItem.alias, tipoItem.nombre, tipoItem.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store Tipo] Algo salio mal al elimar el tipo');
                            reject('Error al elimar el tipo algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarTipoItem(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM tipos_items WHERE id_tipoItem = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store Tipo] Algo ocurrio al eliminar el tipo');
                            reject('Fallo la eliminacion del tipo');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarTipoItem,
    agregarTipoItem,
    actualizarTipoItem,
    eliminarTipoItem
}