const store = require('./store');

function listarTipoItem(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarTipoItem());
    })
}

function agregarTipoItem(tipo){
    return new Promise((resolve, reject)=>{
        if(!tipo.alias || !tipo.nombre){
            console.error('[Controller TipoItem] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullTipoItem = {
            alias : tipo.alias,
            nombre : tipo.nombre
        }
        resolve(store.agregarTipoItem(fullTipoItem));
    }) 
}

function actualizarTipoItem(id, tipo){
    return new Promise((resolve, reject)=>{
        if(!id || !tipo.alias || !tipo.nombre){
            console.error('[Controller TipoItem] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullTipoItem = {
            id,
            alias : tipo.alias,
            nombre : tipo.nombre
        }
        resolve(store.actualizarTipoItem(fullTipoItem));
    })
}

function eliminarTipoItem(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller TipoItem] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarTipoItem(id));
    })
}

module.exports ={
    listarTipoItem,
    agregarTipoItem,
    actualizarTipoItem,
    eliminarTipoItem
}