const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const multer = require('multer');
const imgResize = require('../../network/imgResize');
const Excel = require("exceljs");
var mammoth = require("mammoth");
var pdf = require('html-pdf');
const { TemplateHandler, MimeType } = require('easy-template-x');
const { verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.
const fs = require('fs');
const fileupload = require("express-fileupload");

router.post('/', [verifyToken, agregar],(req, res) => {
  if (!req.files) {
  res.send("File was not found");
  return;
}
 const file = req.files;
  controller.agregarInspe( req.body, req.files).then((result) => {
      response.success(req, res, result, 200);
  }).catch(e => {
      response.error(req, res, 'Error al agregar el item', 400, 'Porfavor verificar agregar item');
  })
})

router.post('/getInspeccion/', verifyToken, (req, res) => {
    controller.cargarInspeccion(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al cargar inpeccion', 500, 'algo ocurrio')
    })
})

router.post('/file/', verifyToken, (req, res) => {
    controller.file(req.body).then((result) => {
        response.success(req, res, result, 200);
        setTimeout(function () {
          console.log("pruebaaa 12333");
            a(result)
        }, 3000);
    }).catch(e => {
        response.error(req, res, 'Error al cargar inpeccion', 500, 'algo ocurrio')
    })
})

router.post('/fileInspeccion/', verifyToken, (req, res) => {
    controller.fileInsp(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al cargar inpeccion', 500, 'algo ocurrio')
    })
})
router.post('/validar/', verifyToken, (req, res) => {
    controller.validar(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al cargar inpeccion', 500, 'algo ocurrio')
    })
})


/**
 *
 * @param {String} nombre
 */
const a = (nombre) => {
    fs.unlinkSync(`./public/formatos/Certificados/${nombre}`);
}

module.exports = router;
