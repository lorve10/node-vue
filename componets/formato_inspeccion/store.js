const {
    conn
} = require('../../settings/db');
const moment = require('moment');
const imgResize = require('../../network/imgResize');
const fs = require('fs').promises;
const fss = require('fs');
const {
    TemplateHandler,
    MimeType
} = require('easy-template-x');
const path = require('path');
const unoconv = require('awesome-unoconv');

const mimeType = require('./mimeType');
const { PDFNet } = require('@pdftron/pdfnet-node');




function agregarInspeccion(item, req){
    return new Promise((resolve, reject)=>{
      conn.getConnection((error, connection)=>{
      if (error) {
        throw error;
      }

      connection.beginTransaction((error)=>{
          if(error){
            throw error;
          }
          connection.query('SELECT id_usuario AS Uid, img AS Imagen FROM usuarios INNER JOIN personas ON id_usuario = id_persona WHERE nombre_usuario = ?', [item.usuario], (error, restl, fields) => {
              if (error) {
                  connection.release(); // Devolvemos la conexion/cerramos conexión.
                  return connection.rollback(() => {
                      throw error;
                  })
              }

              ///funcion random
              const  unico = Math.floor(Math.random() * 100000);
              imgResize.resize2(req.image, `Inspec/${unico+item.item}`, function (err) {
                if (err) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    return connection.rollback(() => {
                        throw err;
                    })
                    // An unknown error occurred when uploading.
                }
              });
              imgResize.resize2(req.imageE, `Inspec/${unico+item.item}`, function (err) {
                if (err) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    return connection.rollback(() => {
                        throw err;
                    })
                    // An unknown error occurred when uploading.
                }
              })
              imgResize.resize2(req.imageB, `Inspec/${unico+item.item}`, function (err) {
                if (err) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    return connection.rollback(() => {
                        throw err;
                    })
                    // An unknown error occurred when uploading.
                }
              })
              console.log(req);
              const nameFile = req.image.name;
              const nameFile2 = req.imageE.name;
              const nameFile3 = req.imageB.name;

              console.log(item.proximaInspec);
              const fechaInsp = item.proximaInspec;
              connection.query('INSERT INTO formato_inspeccion(equipo, marca, fechaInspeccion, vidaUtil, serial, modelo, lote, referencia, fechaFabric, fechaCompra, fechaUso, codigoInterno, proveedor, propietario, departamento, municipio, frenteTrab, clasificacion, apertura, resistencia, usoActual, cumpleStan, otroDatos, mantenimiento, descripManteni, dataTable, otrasObserv, resultado, obserRecome, continuar, idUsuario, idUnicoInspec, imgA, imgB, imgC, item, proximaFecha, id_tipoEquipo, capacidad, puntoAnclaje, resisAnclaje, talla, diametro, longitud, tipoEslinga, clasifiEslinga, numeroConectores, aperturaEslinga, material, dielectrica, materialConstru, otroMaterial, ancho, cargaPermitida, peso, cargaRotura, patasExtendidas, patasRetraidas ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[item.equipo, item.marca, item.fechaIns, item.vidaUtil, item.unico, item.modelo, item.lote, item.referencia, item.fechaFa, item.compra, item.puestaUso, item.codigointe, item.proveedor, item.propietario, item.departamento, item.municipio, item.frenteTraba, item.clasificacion, item.apertura, item.resistencia, item.usoActual, item.cumpleStan, item.otroDatos, item.mantenimiento, item.descripcionM, item.tabla, item.otrasObserv, item.resultado, item.recomendacion, item.continuar,  restl[0].Uid, unico+item.item, nameFile, nameFile2, nameFile3, item.item, fechaInsp, item.id_tipoEquipo, item.capacida, item.puntosAnclaje, item.resistenciaAnclaje, item.Talla, item.diametroCuerda, item.longitud, item.tipoEslinga, item.clasifiEslinga, item.numeroConectores, item.aperturaEslinga, item.material, item.dielectrica, item.materialConstru, item.otroMaterial, item.ancho, item.cargaPermitida, item.peso, item.cargaRotura, item.patasExtendidas, item.patasRetraidas ], (error, rest, fields) =>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    return connection.rollback(() => {
                        throw error;
                    })
                }
                if(item.continuar == "SI"){
                  console.log("entroo");
                  const vigente = "Vigente";
                  connection.query("UPDATE items SET estadoInpe = ? WHERE id_item = ? ", [vigente,  item.item ],(errorr, results, fields)=>{
                    if (errorr) {
                      connection.release(); // Devolvemos la conexion/cerramos conexión.
                        return connection.rollback(() => {
                            throw errorr;
                        })
                    }
                    connection.commit((errorr) => {
                        if (errorr) {
                            return connection.rollback(() => {
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw errorr; // Mensaje de error.
                            });
                          }
                            console.log("guardo exitosamente");
                    });

              });
              connection.query('INSERT INTO certificado_if(codigo, equipo, serial, fechaFabri, marca, cumple, fechaInsp, validez, itemId, firma, userId ) VALUES (?,?,?,?,?,?,?,?,?,?,?)',[item.codigointe, item.equipo, item.unico, item.fechaFa, item.marca, item.cumpleStan, item.fechaIns, fechaInsp, item.item, restl[0].Imagen, restl[0].Uid  ], (er, rest, fields) =>{
                if (er) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    return connection.rollback(() => {
                        throw er;
                    })
                }
                connection.commit((errorr) => {
                    if (errorr) {
                        return connection.rollback(() => {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw errorr; // Mensaje de error.
                        });
                      }
                       //resolve('exito'); //Resolver y devolver la promesa exitosa.

                });
              });
            }

                // // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                // connection.commit((error) => {
                //     connection.release(); // Devolvemos la conexion/cerramos conexión.
                //     if (error) {
                //         return connection.rollback(() => {
                //             throw error; // Mensaje de error.
                //         });
                //     }
            resolve('exito'); //Resolver y devolver la promesa exitosa.
                // });
              })
          });

        })
    })

    })
};

function cargar(item){
        return new Promise((resolve, reject)=>{
          conn.getConnection((error, connection)=>{
          if (error) {
            throw error;
          }
          connection.beginTransaction((error)=>{
              if(error){
                throw error;
              }
              connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [item.user], (error, rest, fields) => {
                  if (error) {
                      connection.release(); // Devolvemos la conexion/cerramos conexión.
                      return connection.rollback(() => {
                          throw error;
                      })
                  }

                  connection.query('SELECT imgC, proximaFecha, cargaPermitida, peso, cargaRotura, patasExtendidas,nombre_persona, patasRetraidas, ancho, materialConstru, otroMaterial, dielectrica, longitud, tipoEslinga, numeroConectores, aperturaEslinga, material,  id_tipoEquipo, capacidad, puntoAnclaje, resisAnclaje, talla, equipo, marca, diametro, fechaInspeccion, vidaUtil, SERIAL, modelo, lote, referencia, fechaFabric, fechaCompra, fechaUso, codigoInterno,propietario, frenteTrab, clasificacion, apertura, resistencia, usoActual, cumpleStan, otroDatos, mantenimiento, descripManteni,dataTable, otrasObserv, resultado, obserRecome, continuar, idUsuario, idUnicoInspec, imgA,imgB, item, fechaCreacion, fechaModificacion,  nombre_proveedor AS proveedor,  nombre_departamento AS departamento, nombre_municipio AS municipio, nombre_marca AS marca FROM formato_inspeccion INNER JOIN proveedores ON proveedor = id_proveedor   INNER JOIN departamentos ON id_departamento = departamento INNER JOIN municipios ON id_municipio = municipio INNER JOIN personas ON idUsuario = id_persona  INNER JOIN marcas ON id_marca = marca WHERE item = ?', [item.numero],(error, results, fields) => {
                      connection.release();
                      if (error) {
                          return connection.rollback(() => {
                              throw error;
                          });
                      }
                      connection.commit((error) => {
                          if (error) {
                              return connection.rollback(() => {
                                  throw error;
                              })
                          }
                          resolve(results);
                      })
                  })

              })
            })


        })
      })
}

function file(item){
    return new Promise( async (resolve, reject)=>{
        let template = '';
        template  = fss.readFileSync('./public/formatos/Certificados/IF08Certificado.docx');

        let img = {}

        if(item.firma > 0 ){
          img = item.imgA;
        }
        let fecha = moment(item.fechaFabri).utc().format('MM/DD/YYYY');
        let fecha2 = moment(item.fechaInsp).utc().format('MM/DD/YYYY');
        console.log(item.firma);
         var res = fss.readFileSync(`./public/FirmaOne/${item.firma}/${item.firma}`);
         console.log(res);
        const data ={
         codigo: item.codigo,
         equipo: item.equipo,
         serial: item.serial,
         fechaFabricacion: fecha,
         marca: item.marca,
         cumple: item.cumple,
         fechaInsp: fecha2,
         validez: item.validez,
         firma: {
             _type: "image",
             source: fss.readFileSync(`./public/FirmaOne/${item.firma}/${item.firma}`),
             format: MimeType.Jpeg,
             width: 200,
             height: 100
         },
        }

        Object.assign(data, img)
        const handler = new TemplateHandler();
        const doc = await handler.process(template, data);
        fss.writeFile(`./public/formatos/Certificados/${item.codigo}.docx`, doc, (err) => {
            if (err) {
                console.log(err);
            }
            resolve(`${item.codigo}.docx`);
        });

        // const inputPath =  `./public/formatos/Certificados/${item.codigo}.docx`
        // const outputPath = `./public/formatos/Certificados/${item.codigo}.pdf`;
        //
        // const convertToPDF = async ()=> {
        //
        //   const pdfdoc = await PDFNet.PDFDoc.create();
        //   await pdfdoc.initSecurityHandler();
        //   await PDFNet.Convert.toPdf(pdfdoc, inputPath);
        //   pdfdoc.save(outputPath, PDFNet.SDFDoc.SaveOptions.e_linearized);
        //   console.log("solo aqui");
        //   resolve(`${item.codigo}.pdf`);
        // }
        // PDFNet.runWithCleanup(convertToPDF, "demo:1645227742451:7b1c04630300000000969091105a895a2411a45b5eafbe74dabaa90a31").then(()=>{
        //     fs.readFile(outputPath, (err, data ) => {
        //         if (err) {
        //             console.log(err);
        //         }else {
        //           console.log("entro a la respuesta");
        //             console.log(data);
        //         }
        //     })
        // }).catch(err=>{
        //   console.log(err)
        // })

    })
};
function fileInsp(item){
    return new Promise( async (resolve, reject)=>{
        let template = '';
        if(item.id_tipoEquipo == 16 || item.id_tipoEquipo == 17 || item.id_tipoEquipo == 18 || item.id_tipoEquipo == 19 || item.id_tipoEquipo == 20 || item.id_tipoEquipo == 21){
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF21 Inspección Tecnica de Arneses de Seguridad V3.docx');
        }else if (item.id_tipoEquipo==75 || item.id_tipoEquipo==148 ) {
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF49 Inspeccion Tecnica de Descendedor Ocho V2.docx');
        }else if( item.id_tipoEquipo == 86 || item.id_tipoEquipo == 88 || item.id_tipoEquipo == 89 || item.id_tipoEquipo == 91 ){
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF45 Inspeccion Tecnica de Eslingas de Posicionamiento-Restriccion V3.docx');
        }else if( item.id_tipoEquipo == 1 || item.id_tipoEquipo == 87 || item.id_tipoEquipo == 90) {
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF44  Inspeccion Tecnica de Eslingas de Detencion V3.docx');
        }else if (item.id_tipoEquipo == 45 || item.id_tipoEquipo == 46 || item.id_tipoEquipo == 47|| item.id_tipoEquipo == 48|| item.id_tipoEquipo == 49) {
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF43 Inspección Tecnica de Casco V3.docx');
        }else if (item.id_tipoEquipo == 50 || item.id_tipoEquipo == 51 || item.id_tipoEquipo == 52|| item.id_tipoEquipo == 53|| item.id_tipoEquipo == 54|| item.id_tipoEquipo == 55
        || item.id_tipoEquipo == 56|| item.id_tipoEquipo == 57|| item.id_tipoEquipo == 58|| item.id_tipoEquipo == 59 ) {
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF42 Inspección Técnica de Cintas o Cuerdas V3.docx');
        }else if (item.id_tipoEquipo == 71 || item.id_tipoEquipo == 73  || item.id_tipoEquipo == 74  || item.id_tipoEquipo == 76 || item.id_tipoEquipo==75  ) {
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF34 Inspeccion Tecnica descencedor ID V3.docx');
        }else if (item.id_tipoEquipo == 72) {
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF30  Inspeccion Tecnica de Descendedor D4 V3.docx');
        }else if(item.id_tipoEquipo==93 || item.id_tipoEquipo == 190){
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF28 Inspeccion Tecnica de Tripode V3.docx');
        }
        else{
          template  = fss.readFileSync('./public/formatos/Inspecciones/IF37 Inspección Técnica de Ganchos y Mosquetones V3.docx');
        }

        let img = {}
        if(item.imgA > 0 ){
          img = item.imgA;
        }
        let material = "";
        let tabla = "";

        if (item.dataTable) {
          tabla = JSON.parse(item.dataTable);
        }
        ///////////// VALIDACION SI EXISTE EL DATO EN MATERIALES TIPO ESLINGAAS
        if(item.material.length > 0 ){
           material = JSON.parse(item.material);
        }
        let clas  = ''; let clas2 = '';
        let si = ''; let no = '';
        let conSi = ""; let conNo="";
        if(item.mantenimiento == "No"){ no = "X"}
        if(item.mantenimiento == "Si"){ si = "X"}
        if (item.clasificacion == "Gancho") { clas = "X" }
        if(item.clasificacion == "Mosquetón"){ clas2  = "X" }
        if(item.continuar == "SI"){ conSi = "x"}
        if (item.continuar == "NO") { conNo = "X"}
        /////////////////////////////////////////////
        let rs = '';
        let ps = '';
        let z  = '';
        let y = '';
        if (item.tipoEslinga == "Restricción"){ rs="X" }
        if (item.tipoEslinga == "Posicionamiento"){ ps="X" }
        if (item.clasifiEslinga == "Doble"){ z="X"}
        if (item.clasifiEslinga == "Sencilla"){ y="X"}
        if (item.clasifiEslinga == "Sencilla"){ y="X"}
        let l = '';
        let i = '';
        if (item.dielectrica == "Si"){ l="X"}
        if (item.dielectrica == "No"){ i="X"}
        let apA = ''; let apB = ''; let apC = '';
        if (item.aperturaEslinga == "25mm" || item.aperturaEslinga == "20mm"){apA ="X"}
        if (item.aperturaEslinga == "45mm"){apB="X"}
        if (item.aperturaEslinga == "65mm"){apC="X"}
        let cr = ""; let rr= ""; let ca =""; let reso=""; let tubular="";
        let ac = ""; let ai = ""; let al = "";
        if(item.material.length > 0 ){
          let materiales = item.material;
            material.forEach((item, i)=>{
              if (item == "Cuerda"){cr="X"}
              if (item == "Reata"){rr="X"}
              if (item == "Cable"){ca="X"}
              if (item == "Resortada"){reso="X"}
              if (item == "Tubular"){tubular = "X"}
              ////////
              if (item == "Acero"){ac = "X"}
              if (item == "Acero Inoxidable"){ai = "X"}
              if (item == "Aluminio"){al = "X"}


            })
        }
        //////////////////////////////////////////////////////////////////////// validacion de datos para eslingas

        const data ={
          ////varibles para Arneses
          capacidad: item.capacidad,
          puntoAnclaje: item.puntoAnclaje,
          resisAnclaje: item.resisAnclaje,
          talla: item.talla,
          ////// variables para eslingas
          longitud: item.longitud,
          rs: rs,
          ps: ps,
          z: z,
          y: y,
          conectores: item.numeroConectores,
          l: l, i: i, apA: apA, apB: apB, apC:apC,
          cr:cr, r:rr, ca:ca, reso:reso , tubular:tubular,
          material: material == null ? "" : material,
          //// clasificacion
          //////
         diametro: item.diametro, ///ocho
         /// para cascos
         materialConst: item.materialConstru,
         otroMaterial: item.otroMaterial,
         /// para cintas
         ancho: item.ancho,
         //// para tripodes o Estructural
         ac: ac, ai:ai, al:al,
         carga: item.cargaPermitida,
         peso: item.peso,
         crou: item.cargaRotura,
         paex: item.patasExtendidas,
         pare: item.patasRetraidas,




         equipo: item.equipo,
         marca: item.marca,
         fechaIns: moment(item.fechaInspeccion).utc().format('MM/DD/YYYY'),
         vidaUtil: item.vidaUtil,
         serial: item.serial,
         modelo: item.modelo,
         lote: item.lote,
         referencia: item.referencia,
         fechaFabri: moment(item.fechaFabric).utc().format('MM/DD/YYYY'),
         fechaCompra: moment(item.fechaCompra).utc().format('MM/DD/YYYY'),
         fechaUso: moment(item.fechaUso).utc().format('MM/DD/YYYY'),
         codigo: item.codigoInterno,
         proveedor: item.proveedor,
         propietario: item.propietario,
         departamento: item.departamento,
         municipio: item.municipio,
         frenteTrabajo: item.frenteTrab,
         c: clas,
         b: clas2,
         si: si ,
         no:  no,
         re: tabla.resul, re2: tabla.resul2, re3:  tabla.resul3, re4: tabla.resul4, re5: tabla.resul5,
         re6: tabla.resul6, re7: tabla.resul7, re8: tabla.resul8, re9: tabla.resul9, re10: tabla.resul10, re11: tabla.resul11,  re12: tabla.resul12, re13: tabla.resul13, re14: tabla.resul14 ,re15: tabla.resul15 ,re16: tabla.resul16,re17: tabla.resul17,re18: tabla.resul18,re19: tabla.resul19,re20: tabla.resul20,
         re21: tabla.resul21, re22: tabla.resul22, re23: tabla.resul23, re24: tabla.resul24, re25: tabla.resul25 ,re26: tabla.resul26,re27: tabla.resul27,re28: tabla.resul28, re29: tabla.resul29, re30: tabla.resul30, re31: tabla.resul31,re32: tabla.resul32, re33: tabla.resul33, re34: tabla.resul34 ,re35: tabla.resul35,
         hal: tabla.hal, hal2: tabla.hal2, hal3: tabla.hal3, hal4: tabla.hal4, hal5: tabla.hal5, hal6: tabla.hal6, hal7: tabla.hal7, hal8: tabla.hal8, hal9: tabla.hal9, hal10: tabla.hal10,
         hal11: tabla.hal11, hal12: tabla.hal12, hal13: tabla.hal13, hal14: tabla.hal14, hal15: tabla.hal15, hal16: tabla.hal16,hal17: tabla.hal17,hal18: tabla.hal18, hal19: tabla.hal19, hal20: tabla.hal20 ,hal21: tabla.hal21, hal22: tabla.hal22,hal23: tabla.hal23,hal24: tabla.hal24,hal25: tabla.hal25,hal26: tabla.hal26, hal27: tabla.hal27,
         hal28: tabla.hal28,hal29: tabla.hal29,hal30: tabla.hal30,hal31: tabla.hal31,hal32: tabla.hal32,hal33: tabla.hal33,hal34: tabla.hal34,hal35: tabla.hal35,
         apertura: item.apertura,
         resistencia: item.resistencia,
         usoActual: item.usoActual,
         cumpleStan: item.cumpleStan,
         otrosDatos: item.otrosDatos,
         mantenimiento: item.mantenimiento,
         descripManteni: item.descripManteni,
         dataTable: item.dataTable,
         otrasObserv: item.otrasObserv,
         resultado: item.resultado,
         obserRecome: item.obserRecome,
         conSi: conSi,
         conNo: conNo,
         imagen: {
             _type: "image",
             source: fss.readFileSync(`./public/img/Inspec/${item.idUnicoInspec}/${item.imgA}`),
             format: MimeType.Jpeg,
             width: 200,
             height: 200
         },
         imagen2: {
             _type: "image",
             source: fss.readFileSync(`./public/img/Inspec/${item.idUnicoInspec}/${item.imgB}`),
             format: MimeType.Jpeg,
             width: 200,
             height: 200
         },
         Imagen3: {
             _type: "image",
             source: fss.readFileSync(`./public/img/Inspec/${item.idUnicoInspec}/${item.imgC}`),
             format: MimeType.Jpeg,
             width: 200,
             height: 200
         },

        }


        Object.assign(data, img)
        const handler = new TemplateHandler();
        const doc = await handler.process(template, data);
        fss.writeFile(`./public/formatos/Inspecciones/equipo_${item.idUnicoInspec}.docx`, doc, (err) => {
            if (err) {
                console.log(err);
            }
            resolve(`Equipo_${item.idUnicoInspec}.docx`);


        });
        // let filename = `${item.equipo}_${item.idUnicoInspec}.docx`;
        //
        // const inputPath = `./public/formatos/Inspecciones/Equipo_${item.idUnicoInspec}.docx`;
        // const outputPath =  `./public/formatos/Inspecciones/Equipo_${item.idUnicoInspec}.pdf`;
        //
        //
        // const convertToPDF = async ()=> {
        //
        //   const pdfdoc = await PDFNet.PDFDoc.create();
        //   await pdfdoc.initSecurityHandler();
        //   await PDFNet.Convert.toPdf(pdfdoc, inputPath);
        //   pdfdoc.save(outputPath, PDFNet.SDFDoc.SaveOptions.e_linearized);
        //   resolve(`Equipo_${item.idUnicoInspec}.pdf`);
        // }
        //
        // PDFNet.runWithCleanup(convertToPDF, "demo:1645227742451:7b1c04630300000000969091105a895a2411a45b5eafbe74dabaa90a31").then(()=>{
        //     fs.readFile(outputPath, (err, data ) => {
        //         if (err) {
        //             console.log(err);
        //         }else {
        //           console.log("res");
        //         }
        //     })
        // }).catch(err=>{
        //   console.log(err)
        // })

    })
}

function validar(item){
  return new Promise((resolve, reject)=>{
    conn.getConnection((error, connection)=>{
    if (error) {
      throw error;
    }
    connection.beginTransaction((error)=>{
        if(error){
          throw error;
        }
        connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [item.user], (error, rest, fields) => {
            if (error) {
                connection.release(); // Devolvemos la conexion/cerramos conexión.
                return connection.rollback(() => {
                    throw error;
                })
            }

            connection.query('SELECT id , proximaFecha,fechaInspeccion, clasificacion FROM formato_inspeccion WHERE  item = ?', [item.id],(error, results, fields) => {
                connection.release();
                if (error) {
                    return connection.rollback(() => {
                        throw error;
                    });
                }
                connection.commit((error) => {
                    if (error) {
                        return connection.rollback(() => {
                            throw error;
                        })
                    }
                    if (results.length > 0 ) {
                      let data = {};
                      results.forEach((item, i) => {
                          if (item.id > i) { data = item }
                      });

                      let actual = moment();
                      let proxima = moment(data.proximaFecha);
                      let dif = actual.diff(proxima, 'days');
                        if(dif > 0 ){
                          let data = {estado: 'Realizar', fecha: moment(proxima).utc().format('MM/DD/YYYY') }
                          resolve(data);

                        }else{
                            let data = {estado: 'Vigente', fecha: moment(proxima).utc().format('MM/DD/YYYY')}
                            resolve(data);
                        }
                    }
                    else{
                      let data = {estado: 'Realizar' }
                      resolve(data);
                    }


                })
            })

        })
      })


  })
})
}




module.exports = {
  agregarInspeccion,
  cargar,
  file,
  fileInsp,
  validar
}
