const store = require('./store');

function agregarInspe(item, req){
    return new Promise((resolve, reject)=>{
              resolve(store.agregarInspeccion(item, req));
    })
}
function cargarInspeccion(item){
    return new Promise((resolve, reject)=>{
              resolve(store.cargar(item));
    })
}
function file(item){
    return new Promise((resolve, reject)=>{
            resolve(store.file(item));
    })
}
function fileInsp(item){
    return new Promise((resolve, reject)=>{
            resolve(store.fileInsp(item));
    })
}
function validar(item) {
    return  new Promise((resolve, reject )=>{
        resolve(store.validar(item))
    })
}

module.exports={
  agregarInspe,
  cargarInspeccion,
  file,
  fileInsp,
  validar
}
