const {conn} = require('../../settings/db'); 

function listarGrupo(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                
                connection.query('SELECT id_grupo AS Uid, concat_ws(" ", nombre_persona, apellido_persona) AS persona, usuarioEncargado_grupo AS Uid_usuario, nombre_grupo AS nombre FROM grupos LEFT JOIN usuarios ON usuarioEncargado_grupo = id_usuario LEFT JOIN personas ON persona_usuario = id_persona', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarGrupo(grupo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('INSERT INTO grupos (usuarioEncargado_grupo, nombre_grupo) VALUES (?, ?)',[grupo.funcionario, grupo.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarGrupo(grupo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE grupos SET  usuarioEncargado_grupo = ?, nombre_grupo = ? WHERE id_grupo = ? ', [grupo.funcionario, grupo.nombre, grupo.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store grupo] Algo salio mal al elimar el grupo');
                            reject('Error al elimar el grupo algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarGrupo(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM grupos WHERE id_grupo = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store grupo] Algo ocurrio al eliminar el grupo');
                            reject('Fallo la eliminacion del grupo');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarGrupo,
    agregarGrupo,
    actualizarGrupo,
    eliminarGrupo
}