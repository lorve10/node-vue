const store = require('./store');

function listarGrupo(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarGrupo());
    })
}

function agregarGrupo(grupo){
    return new Promise((resolve, reject)=>{
        if(!grupo.nombre || !grupo.usuario){
            console.error('[Controller Grupo] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullGrupo = {
            nombre: grupo.nombre,
            funcionario: grupo.usuario
        }
        resolve(store.agregarGrupo(fullGrupo));
    }) 
}

function actualizarGrupo(id, grupo){
    return new Promise((resolve, reject)=>{
        if(!id || !grupo.nombre || !grupo.usuario){
            console.error('[Controller Grupo] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullGrupo = {
            id, 
            nombre: grupo.nombre,
            funcionario: grupo.usuario
        }
        resolve(store.actualizarGrupo(fullGrupo));
    })
}

function eliminarGrupo(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Grupo] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarGrupo(id));
    })
}

module.exports ={
    listarGrupo,
    agregarGrupo,
    actualizarGrupo,
    eliminarGrupo
}