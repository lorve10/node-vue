const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarGrupo().then((allGrupo)=>{
        response.success(req, res, allGrupo, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar a los grupos', 500, 'La consulta a los grupos salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarGrupo(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el grupo', 400, 'Porfavor verificar agregar grupo');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarGrupo(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el grupo', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarGrupo(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el grupo',500,'algo ocurrio al eliminar el grupo')
    })
})

module.exports = router;