const store = require('./store');

function listarMarca(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarMarca());
    })
}

function agregarMarca(marca){
    return new Promise((resolve, reject)=>{
        if(!marca.fabricante || !marca.nombre){
            console.error('[Controller Marca] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullMarca = {
            fabricante : marca.fabricante,
            nombre : marca.nombre
        }
        resolve(store.agregarMarca(fullMarca));
    }) 
}

function actualizarMarca(id, marca){
    return new Promise((resolve, reject)=>{
        if(!id || !marca.fabricante|| !marca.nombre){
            console.error('[Controller Marca] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        fullMarca = {
            id,
            fabricante : marca.fabricante,
            nombre : marca.nombre
        }
        resolve(store.actualizarMarca(fullMarca));
    })
}

function eliminarMarca(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Marca] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarMarca(id));
    })
}


module.exports ={
    listarMarca,
    agregarMarca,
    actualizarMarca,
    eliminarMarca
}