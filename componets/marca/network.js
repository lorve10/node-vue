const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarMarca().then((allMarcas)=>{
        response.success(req, res, allMarcas, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar las marcas', 500, 'La consulta a las marcas salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarMarca(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar la marca', 400, 'Porfavor verificar agregar la marca');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarMarca(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar la marca', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarMarca(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar la marca',500,'algo ocurrio al eliminar la marca')
    })
})

module.exports = router;