const store = require('./store');

function listarItem(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarItem());
    })
}

function listarImagen(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarImagen());
    })
}


function listarVencidos(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarVencido());
    })
}
function agregarItem(req, item){
    return new Promise((resolve, reject)=>{

        let [month, day, year] = item.fechaCompra.split('-')
        let fechaVencimiento = (parseFloat(month) + parseFloat(item.vidaUtil)) + '-' + day + '-' + year;
        if( !item.bodega || !item.proveedor || !item.marca || !item.usuario || !item.tipoItem || !item.fechaCompra || !fechaVencimiento || !item.numeroFactura || !item.valor || !item.vidaUtil || !item.estado){
            console.error('[Controller Item] No se eviaron los campos requeridos1');
            console.log("entro aqui");
            reject('Valida la informacion');
            return false;

        }
        if ((item.tipoItem == 2)) {
          console.log("aquioo tipo 22222222");
            if (!item.tipoEquipo || !item.uso) {
                console.error('[Controller Item] No se eviaron los campos requeridos2');
                console.log("otra prueba");
                reject('Valida la informacion');
                return false;
            }
        }
        if (item.fechaPuestaUso == null) {
          console.log("aquiii");
            item.fechaPuestaUso = null;
        }

        const fullItem = {
            // 'sede':item.sede,
            'bodega':item.bodega,
            'proveedor':item.proveedor,
            'marca':item.marca,
            'usuario':item.usuario,
            'tipoItem':item.tipoItem,
            'codigo':item.codigo,
            'fechaCompra':item.fechaCompra,
            'fechaVencimiento':fechaVencimiento,
            'numeroFactura':item.numeroFactura,
            'descripcion':item.descripcion,
            'modelo':item.modelo,
            'serial':item.serial,
            'lote':item.lote,
            'valor':item.valor,
            'vidaUtil':item.vidaUtil,
            'fechaFabricacion':item.fechaFabricacion,
            'fechaPuestaUso':item.fechaPuestaUso,
            'cumpleNormas':item.cumpleNormas,
            'capacidad':item.capacidad,
            'otrasEspecificacionesTecnicas':item.otrasEspecificacionesTecnicas,
            'manualFabricante':item.manualFabricante,
            'certificaionFabricante':item.certificaionFabricante,
            'estado':item.estado,
            'tipoEquipo':item.tipoEquipo,
            'uso':item.uso,
            'otroUso':item.otroUso,
            'nombreKit':item.nombreKit,
            'informacionKit':item.informacionKit,
            'fechaCreacion': new Date().toISOString().substr(0, 10)
        }

        resolve(store.agregarItem(req, fullItem));
        console.log("esta cojiendo datos");
    })

}

function actualizarItem(req, id, item){
    return new Promise((resolve, reject)=>{
      console.log("entro a editar");
      console.log(item);
        let [month, day, year] = item.fechaCompra.split('-')
        let fechaVencimiento = (parseFloat(month) + parseFloat(item.vidaUtil)) + '-' + day + '-' + year;
        if(!id || !item.bodega || !item.proveedor || !item.marca || !item.usuario || !item.tipoItem || !item.fechaCompra || !fechaVencimiento || !item.numeroFactura || !item.valor || !item.vidaUtil || !item.estado){
            console.error('[Controller Item] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        if ((item.tipoItem == 2)) {
            if (!item.tipoEquipo || !item.uso) {
                console.error('[Controller Item] Verificar los campos alguno esta vacio');
                reject('Algun campo es vacio');
                return false;
            }
        }
        if (item.fechaPuestaUso == null) {
            item.fechaPuestaUso = null;
        }
        const fullItem = {
            id,
            'bodega':item.bodega,
            'proveedor':item.proveedor,
            'marca':item.marca,
            'usuario':item.usuario,
            'tipoItem':item.tipoItem,
            'codigo':item.codigo,
            'fechaCompra':item.fechaCompra,
            'fechaVencimiento':fechaVencimiento,
            'numeroFactura':item.numeroFactura,
            'descripcion':item.descripcion,
            'modelo':item.modelo,
            'serial':item.serial,
            'lote':item.lote,
            'valor':item.valor,
            'vidaUtil':item.vidaUtil,
            'fechaFabricacion':item.fechaFabricacion,
            'fechaPuestaUso':item.fechaPuestaUso,
            'cumpleNormas':item.cumpleNormas,
            'capacidad':item.capacidad,
            'otrasEspecificacionesTecnicas':item.otrasEspecificacionesTecnicas,
            'manualFabricante':item.manualFabricante,
            'certificaionFabricante':item.certificaionFabricante,
            'estado':item.estado,
            'tipoEquipo':item.tipoEquipo,
            'uso':item.uso,
            'otroUso':item.otroUso,
            'nombreKit':item.nombreKit,
            'informacionKit':item.informacionKit,
            'fechaEdicion': new Date().toISOString().substr(0, 10)
        }
        resolve(store.actualizarItem(req, fullItem));
    })
}

function eliminarItem(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Item] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarItem(id));
    })
}

function eliminarItemImage(images){
    return new Promise((resolve, reject)=>{
        if(images.length <= 0){
            console.error('[Controller Item Images] la informacion no llego de las imagenes');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarItemImage(images));
    })
}

function file(item){
    return new Promise((resolve, reject)=>{
        if(item == null){
            console.error('[Controller archivo] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.file(item));
    })
}

module.exports ={
    listarItem,
    listarImagen,
    agregarItem,
    actualizarItem,
    eliminarItem,
    eliminarItemImage,
    file,
    listarVencidos
}
