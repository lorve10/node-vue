const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const multer = require('multer');
const imgResize = require('../../network/imgResize');
const Excel = require("exceljs");
var mammoth = require("mammoth");
var pdf = require('html-pdf');
const {
    TemplateHandler,
    MimeType
} = require('easy-template-x');
const {
    verifyToken,
    agregar,
    editar,
    eliminar
} = require('../../network/valiToken'); //Validacion del token.
const fs = require('fs');
const fileupload = require("express-fileupload");


router.get('/', verifyToken, (req, res) => {
    controller.listarItem().then((allItem) => {
        response.success(req, res, allItem, 200);
    }).catch(e => {
        response.error(req, res, 'No se pudo listar los items', 500, 'La consulta a los items salio mal')
    })
})

router.get('/vencidos', verifyToken, (req, res) => {
    controller.listarItem().then((allItem) => {
        response.success(req, res, allItem, 200);
    }).catch(e => {
        response.error(req, res, 'No se pudo listar los items', 500, 'La consulta a los items salio mal')
    })
})

router.get('/imagen', verifyToken, (req, res) => {
    controller.listarImagen().then((allImagen) => {
        response.success(req, res, allImagen, 200);
    }).catch(e => {
        response.error(req, res, 'No se pudo listar las imagenes', 500, 'La consulta a as imagenes salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res) => {
      if (!req.files) {
      res.send("File was not found");
      return;
    }
    const file = req.files;
        controller.agregarItem( req.files, req.body).then((result) => {
            response.success(req, res, result, 200);
        }).catch(e => {
            response.error(req, res, 'Error al agregar el item', 400, 'Porfavor verificar agregar item');
        })


})

router.post('/itemImagen/', [verifyToken, eliminar], (req, res) => {
    controller.eliminarItemImage(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al eliminar la imagen del item', 500, 'algo ocurrio al eliminar la imagen del item')
    })
})

router.post('/file', [verifyToken, editar], (req, res) => {
    controller.file(req.body).then((result) => {
        response.success(req, res, result, 200);
        console.log("aqui el resultado de la reques");
        console.log(result);
        setTimeout(function () {
            a(result)
        }, 3000);
    }).catch(e => {
        response.error(req, res, 'Error al descargar el archivo', 500, 'algo ocurrio al descargar el archivo')
    })
})

// router.post('/exports', [verifyToken, editar], async (req, res) => {

//     const templateFile = fs.readFileSync('./public/formatos/CF08 Hoja de Vida Equipos de Oficina V1.docx');
//     const data = {
//         imagenUno: {
//             _type: "image",
//             source: fs.readFileSync(`./public/img/Items/1/9388962e436790b6b34ae1f3636cbc34.jpg`),
//             format: MimeType.Jpeg,
//             width: 200,
//             height: 200
//         },
//         nombre: 'freiman',
//     };

//     const handler = new TemplateHandler();
//     const doc = await handler.process(templateFile, data);
//     fs.writeFile(`./public/formatos/myTemplate - output.docx`, doc, (err) => {
//         if (err) {
//             console.log(err);
//         }

//         var options = {
//             convertImage: mammoth.images.imgElement(function (image) {
//                 return image.read("base64").then(function (imageBuffer) {
//                     return {
//                         src: "data:" + image.contentType + ";base64," + imageBuffer
//                     };
//                 });
//             })
//         };

//         mammoth.convertToHtml({
//                 path: "./public/formatos/myTemplate - output.docx"
//             }, options)
//             .then(function (result) {
//                 setTimeout(function () {
//                     a('myTemplate - output.docx')
//                 }, 3000);

//                 var custom_css = `<style>
//             img {
//                 width: 300px;
//             }

//             p {
//                 margin: 0;
//                 font-size: 12px;
//             }

//             td strong {
//                 text-align: left;
//                 color: white;
//                 background-color: #00B0F0;
//                 display: inline-block;
//                 width: 100%;
//             }

//             table {
//                 margin-top: 20px;
//                 width: 100%;
//                 border: 1px solid #000;
//                 border-collapse: collapse;
//             }

//             td {
//                 width: 25%;
//                 text-align: center;
//                 border: 1px solid #000;
//                 border-spacing: 0;
//             }
//         </style>`;

//                 var html = custom_css + result.value; // The generated HTML
//                 pdf.create(html).toFile('./public/formatos/html-pdf.pdf', function (err, res) {
//                     if (err) {
//                         console.log(err);
//                     } else {
//                         console.log(res);
//                     }
//                 });
//             })
//             .done();
//     });

// })


router.post('/export', [verifyToken, editar], (req, res) => {
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('Hoja 1');
    worksheet.autoFilter = {
        from: 'A1',
        to: 'I1',
    }
    worksheet.columns = [{
            header: 'Código',
            key: 'codigo',
            width: 10
        },
        {
            header: 'Descripción',
            key: 'descripcion',
            width: 32
        },
        {
            header: 'Marca',
            key: 'marca',
            width: 32
        },
        {
            header: 'Serial',
            key: 'serial',
            width: 32
        },
        {
            header: 'Valor',
            key: 'valor',
            width: 32
        },
        {
            header: 'Vida Util',
            key: 'vidaUtil',
            width: 32
        },
        {
            header: 'Nombre',
            key: 'nombre',
            width: 32
        },
        {
            header: 'Sede',
            key: 'sede',
            width: 32
        },
        {
            header: 'Estado',
            key: 'estado',
            width: 32
        },
        {
            header: 'Imagen',
            key: '',
            width: 20

        },

    ];

    worksheet.addConditionalFormatting({
        ref: 'A1:J1',
        rules: [{
            type: 'expression',
            richText: [{
                    text: 'This is '
                },
                {
                    font: {
                        italic: true
                    },
                    text: 'italic'
                },
            ],
            formulae: ['MOD(COLUMN(),1)=0'],
            style: {
                fill: {
                    type: 'pattern',
                    pattern: 'solid',
                    bgColor: {
                        argb: '00B0F0'
                    }
                }
            },
        }]
    })

    worksheet.eachRow({
        includeEmpty: true
    }, function (row, rowNumber) {
        row.eachCell(function (cell, colNumber) {
            if (cell.value && cell.value.hyperlink)
                row.getCell(colNumber).font = {
                    color: {
                        argb: "FFFFFF"
                    }
                };
        });
    });

    let i = 1;
    req.body.forEach(element => {
        ++i
        if ((element.archivo != null) && (element.archivo != '')) {
            let imageId = workbook.addImage({
                filename: `./public/img/Items/${element.Uid}/.Metadatos/${element.archivo}`,
                extension: 'jpeg',
            });
            worksheet.addImage(imageId, `J${i}:J${i}`);
        }
        worksheet.addRow({
            codigo: element.codigo,
            descripcion: element.descripcion,
            marca: element.marca,
            serial: element.serial,
            valor: element.valor,
            vidaUtil: element.vidaUtil,
            nombre: `${element.nombre} ${element.apellido}`,
            sede: element.sede,
            estado: element.estado,
        });
        let row = worksheet.lastRow;
        row.height = 80;
    });

    worksheet.eachRow({
        includeEmpty: true
    }, function (row, rowNumber) {
        row.eachCell(function (cell, colNumber) {
            if (cell.value && cell.value.hyperlink)
                console.log(row);
            row.getCell(colNumber).border = {
                top: {
                    style: 'thin',
                    color: {
                        argb: '0000'
                    }
                },
                left: {
                    style: 'thin',
                    color: {
                        argb: '0000'
                    }
                },
                bottom: {
                    style: 'thin',
                    color: {
                        argb: '0000'
                    }
                },
                right: {
                    style: 'thin',
                    color: {
                        argb: '0000'
                    }
                },
            };
        });
    });

    // save under export.xlsx
    workbook.xlsx.writeFile('./public/formatos/export.xlsx').then(() => {
        response.success(req, res, 'export.xlsx', 200);
        setTimeout(function () {
            a('export.xlsx')
        }, 3000);
    }).catch(e => console.log(e));

})

/**
 *
 * @param {String} nombre
 */
const a = (nombre) => {
    fs.unlinkSync(`./public/formatos/${nombre}`);
}

router.post('/:Uid', [verifyToken, editar], (req, res) => {
  console.log("entonces perrooooooo");
  console.log(req.body);
    // imgResize.upload(req, res, function (err) {
    //     if (err instanceof multer.MulterError) {
    //         // A Multer error occurred when uploading.
    //     } else if (err) {
    //         // An unknown error occurred when uploading.
    //     }
        controller.actualizarItem(req.files, req.params.Uid, req.body).then((result) => {
          console.log("entreooooooooooooo");
            response.success(req, res, result, 200)
        }).catch(e => {
            response.error(req, res, 'Algo salio mal al actualizar el item', 500)
        })
    // })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res) => {
    controller.eliminarItem(req.params.Uid).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al eliminar el item', 500, 'algo ocurrio al eliminar el item')
    })
})

module.exports = router;
