const {
    conn
} = require('../../settings/db');
const imgResize = require('../../network/imgResize');
const fs = require('fs').promises;
const fss = require('fs');
const {
    TemplateHandler,
    MimeType
} = require('easy-template-x');
const path = require('path');
const unoconv = require('awesome-unoconv');

const mimeType = require('./mimeType');


function listarItem() {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {

            if (error) {
                throw error;
            }

            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('SELECT id_item AS Uid,estadoInpe, tipoItem_item AS Uid_tipoItem, estado_item AS Uid_estado, nombre_estadoItem AS estado, archivo_imagenItem AS foto, COUNT(item_imagenItem) cantidad, asignacion_item AS asignacion, codigo_item AS codigo, formato_item AS Uid_formato, bodega_item AS Uid_bodega, nombre_bodega AS bodega, tipoEquipo_item AS Uid_tipoEquipo, nombre_tipoEquipo AS tipoEquipo, descripcion_item AS descripcion, marca_item AS Uid_marca, nombre_marca AS marca, fabricante_marca AS Uid_fabricante, nombre_fabricante AS fabricante, nombre_fabricante AS fabricante, proveedor_item AS Uid_proveedor, nombre_proveedor AS proveedor, modelo_item AS modelo, serial_item AS serial, vidaUtil_item AS vidaUtil, sede_bodega AS Uid_sede, nombre_empresa AS empresa, nombre_sede AS sede, DATE_FORMAT(fechaFabricacion_item,"%Y-%m-%d") AS fechaFabricacion, DATE_FORMAT(fechaPuestaUso_item,"%Y-%m-%d") AS fechaUso, uso_item AS Uid_uso, nombre_usoItem AS uso, otroUso_item AS otroUso, nombreKit_item AS kit, informacionKit_item AS informacionKit, cumpleNormas_item AS cumpleNorma, otrasEspecificacionesTecnicas_item AS especificaciones, valor_item AS valor, numeroFactura_item AS factura, capacidad_item AS capacidad, lote_item AS lote, DATE_FORMAT(fechaCompra_item,"%Y-%m-%d") AS fechaCompra, DATE_FORMAT(fechaVencimiento_item,"%Y-%m-%d") AS vencimiento, manualFabricante_item AS manual, certificacionFabricante_item AS certificacion, usuario_item AS Uid_usuario, persona_usuario AS Uid_persona, nombre_persona AS nombre, apellido_persona AS apellido,count(codigo_item) cantidad, periodicidadInspeccion_item AS inspeccion, periodicidadMantenimiento_item AS mantenimiento, periodicidadAlmacenamiento_item AS almacenamiento FROM items INNER JOIN bodegas ON bodega_item = id_bodega INNER JOIN sedes ON id_sede = sede_bodega LEFT JOIN tipos_equipos ON id_tipoEquipo = tipoEquipo_item INNER JOIN empresas ON id_empresa = empresa_sede INNER JOIN proveedores ON proveedor_item = id_proveedor INNER JOIN marcas ON marca_item = id_marca LEFT JOIN fabricantes ON id_fabricante = fabricante_marca LEFT JOIN items_asignaciones ON ((id_item = item_asignacion)AND(estado_asignacion = 1)) LEFT JOIN usuarios ON usuario_asignacion = id_usuario LEFT JOIN personas ON persona_usuario = id_persona INNER JOIN tipos_items ON tipoItem_item = id_tipoItem INNER JOIN estados_items ON estado_item = id_estadoItem INNER JOIN usos_items ON uso_item = id_usoItem LEFT JOIN imagenes_items ON (id_item = item_imagenItem) group by codigo_item ORDER BY fechaVencimiento_item DESC', (error, results, fields) => {
                    connection.release();
                    if (error) {
                        return connection.rollback(() => {
                            throw error;
                        });
                    }
                    connection.commit((error) => {
                        if (error) {
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function listarVencido() {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {

            if (error) {
                throw error;
            }

            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('SELECT id_item AS Uid,estadoInpe, tipoItem_item AS Uid_tipoItem, estado_item AS Uid_estado, nombre_estadoItem AS estado, archivo_imagenItem AS foto, COUNT(item_imagenItem) cantidad, asignacion_item AS asignacion, codigo_item AS codigo, formato_item AS Uid_formato, bodega_item AS Uid_bodega, nombre_bodega AS bodega, tipoEquipo_item AS Uid_tipoEquipo, nombre_tipoEquipo AS tipoEquipo, descripcion_item AS descripcion, marca_item AS Uid_marca, nombre_marca AS marca, fabricante_marca AS Uid_fabricante, nombre_fabricante AS fabricante, nombre_fabricante AS fabricante, proveedor_item AS Uid_proveedor, nombre_proveedor AS proveedor, modelo_item AS modelo, serial_item AS serial, vidaUtil_item AS vidaUtil, sede_bodega AS Uid_sede, nombre_empresa AS empresa, nombre_sede AS sede, DATE_FORMAT(fechaFabricacion_item,"%Y-%m-%d") AS fechaFabricacion, DATE_FORMAT(fechaPuestaUso_item,"%Y-%m-%d") AS fechaUso, uso_item AS Uid_uso, nombre_usoItem AS uso, otroUso_item AS otroUso, nombreKit_item AS kit, informacionKit_item AS informacionKit, cumpleNormas_item AS cumpleNorma, otrasEspecificacionesTecnicas_item AS especificaciones, valor_item AS valor, numeroFactura_item AS factura, capacidad_item AS capacidad, lote_item AS lote, DATE_FORMAT(fechaCompra_item,"%Y-%m-%d") AS fechaCompra, DATE_FORMAT(fechaVencimiento_item,"%Y-%m-%d") AS vencimiento, manualFabricante_item AS manual, certificacionFabricante_item AS certificacion, usuario_item AS Uid_usuario, persona_usuario AS Uid_persona, nombre_persona AS nombre, apellido_persona AS apellido,count(codigo_item) cantidad, periodicidadInspeccion_item AS inspeccion, periodicidadMantenimiento_item AS mantenimiento, periodicidadAlmacenamiento_item AS almacenamiento FROM items INNER JOIN bodegas ON bodega_item = id_bodega INNER JOIN sedes ON id_sede = sede_bodega LEFT JOIN tipos_equipos ON id_tipoEquipo = tipoEquipo_item INNER JOIN empresas ON id_empresa = empresa_sede INNER JOIN proveedores ON proveedor_item = id_proveedor INNER JOIN marcas ON marca_item = id_marca LEFT JOIN fabricantes ON id_fabricante = fabricante_marca LEFT JOIN items_asignaciones ON ((id_item = item_asignacion)AND(estado_asignacion = 1)) LEFT JOIN usuarios ON usuario_asignacion = id_usuario LEFT JOIN personas ON persona_usuario = id_persona INNER JOIN tipos_items ON tipoItem_item = id_tipoItem INNER JOIN estados_items ON estado_item = id_estadoItem INNER JOIN usos_items ON uso_item = id_usoItem LEFT JOIN imagenes_items ON (id_item = item_imagenItem) WHERE estadoInpe = "Vencido" group by codigo_item ORDER BY fechaVencimiento_item DESC', (error, results, fields) => {
                    connection.release();
                    if (error) {
                        return connection.rollback(() => {
                            throw error;
                        });
                    }
                    connection.commit((error) => {
                        if (error) {
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function listarImagen() {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {

            if (error) {
                throw error;
            }

            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }

                connection.query('SELECT item_imagenItem AS Uid_item, archivo_imagenItem AS foto, tipoImagen_imagenItem FROM imagenes_items INNER JOIN items ON item_imagenItem = id_item ', (error, results, fields) => {
                    connection.release();
                    if (error) {
                        return connection.rollback(() => {
                            throw error;
                        });
                    }
                    connection.commit((error) => {
                        if (error) {
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarItem(req, item) {
  console.log(item);
  console.log("entro al agregar");
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [item.usuario], (error, rest, fields) => {
                    if (error) {
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        return connection.rollback(() => {
                            throw error;
                        })
                    }
                    var tipoEquipo = '';
                    if (item.tipoEquipo == 'null') {
                        tipoEquipo = null;
                    } else {
                        tipoEquipo = item.tipoEquipo;
                    }

                    const nameFile = req.image.name;
                    const nameFile2 = req.imageE.name;

                    connection.query('INSERT INTO items (bodega_item, proveedor_item, marca_item, usuario_item, tipoItem_item, codigo_item, fechaCompra_item, fechaVencimiento_item, numeroFactura_item, descripcion_item, modelo_item, serial_item, lote_item, valor_item, vidaUtil_item, fechaFabricacion_item, fechaPuestaUso_item, cumpleNormas_item, capacidad_item, otrasEspecificacionesTecnicas_item, manualFabricante_item, certificacionFabricante_item, estado_item, tipoEquipo_item, uso_item, otroUso_item, nombreKit_item, informacionKit_item, fechaCrea_item, fechaEdit_item, imageA, imageB) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [item.bodega, item.proveedor, item.marca, rest[0].Uid, item.tipoItem, item.codigo, item.fechaCompra, item.fechaVencimiento, item.numeroFactura, item.descripcion, item.modelo, item.serial, item.lote, item.valor, item.vidaUtil, item.fechaFabricacion, item.fechaPuestaUso, item.cumpleNormas, item.capacidad, item.otrasEspecificacionesTecnicas, item.manualFabricante, item.certificaionFabricante, item.estado, tipoEquipo, item.uso, item.otroUso, item.nombreKit, item.informacionKit, item.fechaCreacion, item.fechaCreacion,nameFile, nameFile2], (error, results, fields) => {
                        if (error) {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        const itemId = results.insertId;

                        imgResize.resize2(req.image, `Items/${itemId}`, function (err) {
                          console.log("entrooo a guardar imagen");
                            if (err) {
                              console.log("entro al error");
                              console.log(err);
                            }
                            connection.query('INSERT INTO imagenes_items (item_imagenItem, tipoImagen_imagenItem, nombre_imagenItem, archivo_imagenItem, fechaCreacion_imagenItem, fechaModificacion_imagenItem) VALUES (?, ?, ?, ?, ?, ?)', [itemId, 'G', req.image.name,  req.image.name, item.fechaCreacion, item.fechaCreacion], (errorr, results, fields) => {
                              console.log("guardo exitosamente oooooooe");

                                      if (errorr) {
                                          connection.release(); // Devolvemos la conexion/cerramos conexión.
                                          //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                          return connection.rollback(() => {
                                              throw errorr; // Mensaje de error.
                                          });
                                      }
                                      connection.commit((errorr) => {
                                          if (errorr) {
                                              return connection.rollback(() => {
                                                  connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                  throw errorr; // Mensaje de error.
                                              });
                                            }
                                              console.log("guardo exitosamente");
                                      });

                              });
                        });
                        imgResize.resize2(req.imageE, `Items/${itemId}`, function (err) {
                            if (err) {
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                return connection.rollback(() => {
                                    throw err;
                                })
                                // An unknown error occurred when uploading.
                            }
                            console.log("guardo exitosamente prueba");

                            connection.query('INSERT INTO imagenes_items (item_imagenItem, tipoImagen_imagenItem, nombre_imagenItem, archivo_imagenItem, fechaCreacion_imagenItem, fechaModificacion_imagenItem) VALUES (?, ?, ?, ?, ?, ?)', [itemId, 'E', req.imageE.name, req.imageE.name, item.fechaCreacion, item.fechaCreacion], (errorr, results, fields) => {
                              console.log("entro a guardar");
                                        if (errorr) {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                            return connection.rollback(() => {
                                                throw errorr; // Mensaje de error.
                                            });
                                        }
                                        connection.commit((errorr) => {
                                            if (errorr) {
                                                return connection.rollback(() => {
                                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                    throw errorr; // Mensaje de error.
                                                });
                                              }
                                               //resolve('exito'); //Resolver y devolver la promesa exitosa.

                                        });
                                })

                        })



                        connection.commit((error) => {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            if (error) {
                                return connection.rollback(() => {
                                    throw error; // Mensaje de error.
                                });
                            }
                            resolve('exito'); //Resolver y devolver la promesa exitosa.
                        });
                        // })
                    })
                })
            })
        })
    })
}

function eliminarItemImage(imagen) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                imagen.forEach((element, index) => {
                    ++index;
                    connection.query('DELETE FROM imagenes_items WHERE (item_imagenItem = ?) AND (archivo_imagenItem = ?) ', [element.Uid_item, element.foto], (error, results, fields) => {
                        if (error) {
                            connection.release();
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        fs.unlink(`./public/img/Items/${element.Uid_item}/.Metadatos/${element.foto}`).then(() => {
                            fs.unlink(`./public/img/Items/${element.Uid_item}/${element.foto}`).then(() => {
                                if (index == imagen.length) {
                                    connection.commit((error) => {
                                        if (error) {
                                            return connection.rollback(() => {
                                                throw error;
                                            })
                                        } else if (results.affectedRows) {
                                            resolve('exito');
                                        } else {
                                            console.error('[store item] Algo ocurrio al eliminar el item');
                                            reject('Fallo la eliminacion del item');
                                            return false;
                                        }
                                    });
                                }
                            }).catch(err => {
                                console.error('Something wrong happened removing the file', err)
                            })
                        }).catch(err => {
                            console.error('Something wrong happened removing the file', err)
                        })
                    });
                });
            })
        })
    })
}

function actualizarItem(req, item) {
  console.log(item);
  console.log("entro al agregar");
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [item.usuario], (error, rest, fields) => {
                    if (error) {
                        return connection.rollback(() => {
                          connection.release(); // Devolvemos la conexion/cerramos conexión.

                            throw error;
                        })
                    }
                    var tipoEquipo = '';
                    if (item.tipoEquipo == 'null') {
                        tipoEquipo = null;
                    } else {
                        tipoEquipo = item.tipoEquipo;
                    }

                    const nameFile = req.image.name;
                    const nameFile2 = req.imageE.name;

                    connection.query('UPDATE items SET bodega_item = ?, proveedor_item = ?, marca_item = ?, usuarioModi_item = ?, tipoItem_item = ?, codigo_item = ?, fechaCompra_item = ?, fechaVencimiento_item = ?, numeroFactura_item = ?, descripcion_item = ?, modelo_item = ?, serial_item = ?, lote_item = ?, valor_item = ?, vidaUtil_item = ?, fechaFabricacion_item = ?, fechaPuestaUso_item = ?, cumpleNormas_item = ?, capacidad_item = ?, otrasEspecificacionesTecnicas_item = ?, manualFabricante_item = ?, certificacionFabricante_item = ?, estado_item = ?, tipoEquipo_item = ?, uso_item = ?, otroUso_item = ?, nombreKit_item = ?, informacionKit_item = ?, fechaEdit_item = ? WHERE id_item = ? ', [item.bodega, item.proveedor, item.marca, rest[0].Uid, item.tipoItem, item.codigo, item.fechaCompra, item.fechaVencimiento, item.numeroFactura, item.descripcion, item.modelo, item.serial, item.lote, item.valor, item.vidaUtil, item.fechaFabricacion, item.fechaPuestaUso, item.cumpleNormas, item.capacidad, item.otrasEspecificacionesTecnicas, item.manualFabricante, item.certificaionFabricante, item.estado, tipoEquipo, item.uso, item.otroUso, item.nombreKit, item.informacionKit, item.fechaEdicion, item.id], (error, results, fields) => {
                        if (error) {
                            return connection.rollback(() => {
                              connection.release(); // Devolvemos la conexion/cerramos conexión.

                                throw error;
                            })
                        }


                        imgResize.resize2(req.image, `Items/${item.id}`, function (err) {
                          console.log("entrooo a guardar imagen");
                            if (err) {
                              console.log("entro al error");
                              console.log(err);
                            }
                            connection.query('INSERT INTO imagenes_items (item_imagenItem, tipoImagen_imagenItem, nombre_imagenItem, archivo_imagenItem) VALUES (?, ?, ?, ?)', [item.id, 'G', req.image.name, req.image.name], (errorr, results, fields) => {

                                      if (errorr) {
                                          //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                          connection.release(); // Devolvemos la conexion/cerramos conexión.

                                          return connection.rollback(() => {

                                              throw errorr; // Mensaje de error.
                                          });
                                      }
                                      connection.commit((errorr) => {
                                          if (errorr) {
                                              return connection.rollback(() => {
                                                  connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                  throw errorr; // Mensaje de error.
                                              });
                                            }
                                              console.log("guardo exitosamente");
                                      });

                              });
                        });
                        imgResize.resize2(req.imageE, `Items/${item.id}`, function (err) {
                            if (err) {
                                return connection.rollback(() => {
                                  connection.release(); // Devolvemos la conexion/cerramos conexión.

                                    throw err;
                                })
                                // An unknown error occurred when uploading.
                            }
                            console.log("guardo exitosamente prueba");

                            connection.query('INSERT INTO imagenes_items (item_imagenItem, tipoImagen_imagenItem, nombre_imagenItem, archivo_imagenItem) VALUES (?, ?, ?, ?)', [item.id, 'E', req.imageE.name, req.imageE.name], (errorr, results, fields) => {
                              console.log("entro a guardar");
                                        if (errorr) {

                                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                            return connection.rollback(() => {
                                              connection.release(); // Devolvemos la conexion/cerramos conexión.

                                                throw errorr; // Mensaje de error.
                                            });
                                        }
                                        connection.commit((errorr) => {
                                            if (errorr) {
                                                return connection.rollback(() => {
                                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                    throw errorr; // Mensaje de error.
                                                });
                                              }
                                               //resolve('exito'); //Resolver y devolver la promesa exitosa.

                                        });
                                })

                        })



                        connection.commit((error) => {
                            if (error) {
                                return connection.rollback(() => {
                                    throw error; // Mensaje de error.
                                    connection.release(); // Devolvemos la conexion/cerramos conexión.

                                });
                            }
                            resolve('exito'); //Resolver y devolver la promesa exitosa.
                        });
                        // })
                    })
                })
            })
        })
    })
}

function eliminarItem(id) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }

                connection.query('DELETE FROM items WHERE id_item = ?', [id], (error, results, fields) => {
                    connection.release();
                    if (error) {
                        return connection.rollback(() => {
                            throw error;
                        })
                    }
                    fs.rmdir(`./public/img/Items/${id}`, {
                        recursive: true
                    })
                        .then(() => {
                            connection.commit((error) => {
                                if (error) {
                                    return connection.rollback(() => {
                                        throw error;
                                    })
                                } else if (results.affectedRows) {

                                    resolve('exito');
                                } else {
                                    console.error('[store item] Algo ocurrio al eliminar el item');
                                    reject('Fallo la eliminacion del item');
                                    return false;
                                }
                            });
                        })
                        .catch(err => {
                            console.error('Error al borrar los archivos del item', err);
                        })
                });
            })
        })
    })
}

function file(item) {
    return new Promise(async (resolve, reject) => {
        // 1. read template file
        let templateFile = '';
        if (item.Uid_tipoItem == 2) {
            templateFile = fss.readFileSync('./public/formatos/HojaVidaAltoRiesgo.docx');
        } else {
            templateFile = fss.readFileSync('./public/formatos/CF08 Hoja de Vida Equipos de Oficina V1.docx');
        }
        const imagenUno = item.foto.filter(function (element) {
            return element.tipoImagen_imagenItem == 'G';
        });
        const imagenDos = item.foto.filter(function (element) {
            return element.tipoImagen_imagenItem == 'E';
        });

        console.log("entro el item perrito");
        console.log(item);
        let a = {}
        if (imagenDos.length > 0) {
            a = {
                imagenDos: {
                    _type: "image",
                    source: fss.readFileSync(`./public/img/Items/${item.Uid}/${imagenDos[0].foto}`),
                    format: MimeType.Jpeg,
                    width: 200,
                    height: 200
                }
            }
        }

        // 2. process the template
        const data = {
            imagenUno: {
                _type: "image",
                source: fss.readFileSync(`./public/img/Items/${item.Uid}/${imagenUno[0].foto}`),
                format: MimeType.Jpeg,
                width: 200,
                height: 200
            },
            bodega: item.bodega,
            capacidad: item.capacidad,
            certificado: item.certificacion,
            certificaion: item.cumpleNorma,
            codigo: item.codigo,
            empresa: item.empresa,
            entrenamiento: item.Uid_uso,
            especificacion: item.especificaciones,
            fabricante: item.fabricante,
            factura: item.factura,
            fCompra: item.fechaCompra,
            fFabricacion: item.fechaFabricacion,
            fUso: item.fechaUso,
            fVencimiento: item.vencimiento,
            informacionKit: item.informacionKit,
            kit: item.kit,
            lote: item.lote,
            manual: item.manual,
            marca: item.marca,
            modelo: item.modelo,
            noCertificado: item.certificacion,
            noManual: item.manual,
            nombre: item.descripcion,
            otroUso: item.otroUso,
            pInspeccion: item.inspeccion,
            pMantenimiento: item.mantenimiento,
            pAlmacenamiento: item.almacenamiento,
            propietario: item.empresa,
            proveedor: item.proveedor,
            rescate: item.Uid_uso,
            seguridad: item.Uid_uso,
            serial: item.serial,
            tipoEquipo: item.tipoEquipo,
            ubicacion: item.sede,
            uso: item.uso,
            valor: item.valor,
            vidaUtil: item.vidaUtil,
        };

        Object.assign(data, a);

        if (item.manual == 1) {
            data.manual = 'X';
            data.noManual = '';
        } else {
            data.manual = 'x';
            data.noManual = 'X';
        }

        if (item.certificacion == 1) {
            data.certificado = 'X';
            data.noCertificado = '';
        } else {
            data.certificado = '';
            data.noCertificado = 'X';
        }

        if (item.Uid_uso == 1) {
            data.seguridad = 'X';
            data.entrenamiento = '';
            data.rescate = '';
        } else if (item.Uid_uso == 2) {
            data.rescate = 'X';
            data.entrenamiento = '';
            data.seguridad = '';
        } else if (item.Uid_uso == 3) {
            data.entrenamiento = 'X';
            data.rescate = '';
            data.seguridad = '';
        }

        const handler = new TemplateHandler();
        const doc = await handler.process(templateFile, data);
        fss.writeFile(`./public/formatos/equipo_${item.codigo}.docx`, doc, (err) => {
            if (err) {
                console.log(err);
            }
            resolve(`equipo_${item.codigo}.docx`);
        });

    })
}

module.exports = {
    listarItem,
    listarImagen,
    agregarItem,
    actualizarItem,
    eliminarItem,
    eliminarItemImage,
    file,
    listarVencido
}
