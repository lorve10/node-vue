const {conn} = require('../../settings/db')

function listarDocumento(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_documento AS Uid, item_documento AS item, nombre_documento AS nombre, archivo_documento AS archivo, archivo_documentoImagen AS imagen FROM documentos INNER JOIN documentos_imagenes ON id_documento = id_documentoImagen;',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarDocumento(formato){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO formatos (plantilla_formato, nombre_formato) VALUES (?, ?)', [formato.plantilla, formato.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function ordenarDocumento(data){
    
    return new Promise((resolve, reject)=>{
    //     conn.getConnection((error, connection)=>{
    //         if(error){
    //             throw error;
    //         }
    //         connection.beginTransaction((error)=>{
    //             if(error){throw error;}
    //             connection.query('INSERT INTO formatos (plantilla_formato, nombre_formato) VALUES (?, ?)', [formato.plantilla, formato.nombre], (error, results, fields)=>{
    //                 connection.release();
    //                 if(error){
    //                     return connection.rollback(()=>{
    //                         throw error;
    //                     })
    //                 }
    //                 connection.commit((error)=>{
    //                     if(error){
    //                         return connection.rollback(()=>{
    //                             throw error;
    //                         })
    //                     }
    //                     connection.commit((error)=>{
    //                         if(error){
    //                             return connection.rollback(()=>{
    //                                 throw error;
    //                             })
    //                         }
    //                         resolve('exito');
                            resolve(data);
    //                     })
    //                 });
    //             })
    //         })
    //     })
    })
}

function actualizarDocumento(formato){
    return new Promise ((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error}
                connection.query('UPDATE formatos SET plantilla_formato = ?, nombre_formato = ? WHERE id_formato = ?', [formato.plantilla, formato.nombre, formato.id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{throw error;})
                    }

                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{throw error;})
                        }
                        else if(results.affectedRows){
                            resolve('exito');
                        }
                        else {
                            console.error('No se pudo actualizar el formato');
                            reject('Algho salio mal al actualizar el formato');
                            return false;
                        }
                    });

                })
            })
        })
    })
}

function eliminarDocumento(id){
    return new Promise((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){ throw error; }
                connection.query('DELETE FROM formatos WHERE id_formato = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){ 
                        connection.rollback(()=>{ 
                        throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){connection.rollback(()=>{
                                throw error
                            })
                        }
                        else if(results.affectedRows){
                            resolve('exito')
                        }else{
                            console.error('No se pudo eliminar el formato');
                            reject('Intenta nuevamente eliminar el formato')
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarDocumento,
    agregarDocumento,
    ordenarDocumento,
    actualizarDocumento,
    eliminarDocumento
}