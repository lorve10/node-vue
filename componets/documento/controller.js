const store = require('./store');

function listarDocumento(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarDocumento());
    })
}
function agregarDocumento(formato){
    return new Promise((resolve, reject)=>{
        if(!formato.plantillaFormato || !formato.nombreformato){
            console.error('[Controller formato] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullFormato = {
            plantilla : formato.plantillaFormato,
            nombre : formato.nombreformato
        }
        resolve(store.agregarDocumento(fullFormato));
    })
}

function ordenarDocumento(data){
    return new Promise((resolve, reject)=>{
        if((data.fil.length <= 0) || (data.doc.length <= 0) ){
            console.error('[Controller formato] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        resolve(store.ordenarDocumento(data));
    })
}
function actualizarDocumento(id, formato){
    return new Promise((resolve, reject)=>{
        if(!id || !formato.plantillaFormato || !formato.nombreFormato){
            console.error('[Controller formato]');
            reject('');
            return false;
        }
        const fullFormato = { 
            id, 
            plantilla : formato.plantillaFormato,
            nombre : formato.nombreFormato
        }
        resolve(store.actualizarDocumento(fullFormato))
    })
}
function eliminarDocumento(id){
    return new Promise((resolve, reject)=>{
        if (!id) {
            console.error('[Controller formato] el identificador del formato es vacio');
            reject('Verificar campo llego vacio');
            return false;
        }
        resolve(store.eliminarDocumento(id));
    })
}

module.exports = {
    listarDocumento,
    agregarDocumento,
    ordenarDocumento,
    actualizarDocumento,
    eliminarDocumento
}