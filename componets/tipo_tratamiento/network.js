const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarTratamiento().then((allTratamiento)=>{
        response.success(req, res, allTratamiento, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar alos tratamientos', 500, 'La consulta a los tratamientos salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarTratamiento(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el tratamiento', 400, 'Porfavor verificar agregar tratamiento');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarTratamiento(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el tratamiento', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarTratamiento(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el tratamiento',500,'algo ocurrio al eliminar el tratamiento')
    })
})

module.exports = router;