const store = require('./store');

function listarTratamiento(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarTratamiento());
    })
}

function agregarTratamiento(tipo){
    return new Promise((resolve, reject)=>{
        if(!tipo.nombre){
            console.error('[Controller Tratamiento] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        resolve(store.agregarTratamiento(tipo));
    }) 
}

function actualizarTratamiento(id, tipo){
    return new Promise((resolve, reject)=>{
        if(!id || !tipo.nombre){
            console.error('[Controller Tratamiento] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullTratamiento = {
            id, 
            nombre: tipo.nombre
        }
        resolve(store.actualizarTratamiento(fullTratamiento));
    })
}

function eliminarTratamiento(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Tratamiento] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarTratamiento(id));
    })
}

module.exports ={
    listarTratamiento,
    agregarTratamiento,
    actualizarTratamiento,
    eliminarTratamiento
}