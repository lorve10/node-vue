const {conn} = require('../../settings/db'); 

function listarTratamiento(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                
                connection.query('SELECT id_tipoTratamiento AS Uid, nombre_tipoTratamiento AS nombre FROM tipos_tratamientos', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarTratamiento(tipo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('INSERT INTO tipos_tratamientos (nombre_tipoTratamiento) VALUES (?)',[tipo.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                    })
                })
            })
        })
    })
}

function actualizarTratamiento(tratamiento){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE tipos_tratamientos SET nombre_tipoTratamiento = ? WHERE id_tipoTratamiento = ? ', [tratamiento.nombre, tratamiento.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store Tratamiento] Algo salio mal al elimar el tratamiento');
                            reject('Error al elimar el Tratamiento algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarTratamiento(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM tipos_tratamientos WHERE id_tipoTratamiento = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store Tratamiento] Algo ocurrio al eliminar el tratamiento');
                            reject('Fallo la eliminacion del tratamiento');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarTratamiento,
    agregarTratamiento,
    actualizarTratamiento,
    eliminarTratamiento
}