const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarFabricante().then((allFabricante)=>{
        response.success(req, res, allFabricante, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar los fabrivcantes', 500, 'La consulta a los fabricantes salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarFabricante(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el fabricante', 400, 'Porfavor verificar agregar fabricante');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarFabricante(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el fabricante', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarFabricante(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el fabricante',500,'algo ocurrio al eliminar el fabricante')
    })
})

module.exports = router;