const store = require('./store');

function listarFabricante(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarFabricante());
    })
}

function agregarFabricante(fabricante){
    return new Promise((resolve, reject)=>{
        if(!fabricante.nombre){
            console.error('[Controller Fabricante] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        resolve(store.agregarFabricante(fabricante));
    }) 
}

function actualizarFabricante(id, fabricante){
    return new Promise((resolve, reject)=>{
        if(!id || !fabricante.nombre){
            console.error('[Controller Fabricante] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullFabricante = {
            id, 
            nombre: fabricante.nombre
        }
        resolve(store.actualizarFabricante(fullFabricante));
    })
}

function eliminarFabricante(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Fabricante] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarFabricante(id));
    })
}

module.exports ={
    listarFabricante,
    agregarFabricante,
    actualizarFabricante,
    eliminarFabricante
}