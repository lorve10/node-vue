// Store usuario.
const {
    conn
} = require('../../settings/db'); // Llama al archivo de conexion.
const bcrypt = require('bcrypt'); // Llama al sistema de encriptacion de contraseña.
const saltRounds = 8; // Configracion de la encriptacion de la cotraseña.

const moment = require('moment');
const imgResize = require('../../network/imgResize');
const fs = require('fs').promises;
const fss = require('fs');
const {
    TemplateHandler,
    MimeType
} = require('easy-template-x');
const path = require('path');
const unoconv = require('awesome-unoconv');

const { PDFNet } = require('@pdftron/pdfnet-node');





// Funcion para agregar una persona.
function add(file, persona) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject) => {

        // Abrimos una conexion a la base de datos.
        conn.getConnection((errC, connection) => {
            if (errC) {
                throw errC;
            } // Mensaje de error.
            // Inicio de transacción.
            connection.beginTransaction(function (err) {
                if (err) {
                    throw err;
                } // Mensaje de error.

                ///let fecha = moment(persona.fecha).utc().format('MM/DD/YYYY');

                  imgResize.resize3(file.image, `FirmaOne/${persona.identificacion}`, function (err) {
                    if (err) {
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        return connection.rollback(() => {
                            throw err;
                        })
                        // An unknown error occurred when uploading.
                    }
                  })
                let name = persona.identificacion;

                console.log("aqui la imagen");
                connection.query('INSERT INTO personas (tratamiento_persona, municipioNacimiento_persona, nombre_persona, apellido_persona, identificacion_persona, fechaNacimiento_persona, domicilio_persona, telefono_persona, correo_persona, img, fechaCreacion_persona) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [persona.tratamiento, persona.municipio, persona.nombre, persona.apellido, persona.identificacion, persona.nacimiento, persona.direccion, persona.telefono, persona.email, name, persona.fecha], (error, results, fields) => {
                    if (error) {
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                        return connection.rollback(() => {
                            throw error; // Mensaje de error.
                        });
                    }
                    // Encriptación de contraseña (Fase1)
                    bcrypt.genSalt(saltRounds, function (erro, salt) {
                        if (erro) {
                            throw erro;
                        } // Mensaje de error.
                        // Encriptación de contraseña (Fase2)
                        bcrypt.hash(persona.contrasena, salt, function (erre, hash) {
                            if (erre) {
                                throw erre;
                            } // Mensaje de error.
                            connection.query('INSERT INTO usuarios (persona_usuario, grupo_usuario, cargo_usuario, sede_usuario, rol_usuario, estado_usuario, nombre_usuario, contrasena_usuario, telefono_usuario, correo_usuario, fechaCreacion_usuario) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [results.insertId, persona.grupo, persona.cargo, persona.sede, persona.rol, persona.estado, persona.usuario, hash, persona.telefonoC, persona.emailC, persona.fecha], (errorr, results, fields) => {
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                if (errorr) {
                                    //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                    return connection.rollback(() => {
                                        throw errorr; // Mensaje de error.
                                    });
                                }
                                // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                                connection.commit((errOm) => {
                                    if (errOm) {
                                        return connection.rollback(() => {
                                            throw errOm; // Mensaje de error.
                                        });
                                    }
                                    resolve('exito'); //Resolver y devolver la promesa exitosa.
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

//Function para listar todas las personas
function list() {
    // Devolucion de una promesa.
    return new Promise((resolve, reject) => {
        // Abrimos una conexion a la base de datos.
        conn.getConnection((err, connection) => {
            // Inicio de transacción.
            connection.beginTransaction((err) => {
                if (err) {
                    throw err;
                } // Mensaje de error.
                // connection.query('SELECT id_persona AS Uid, id_usuario AS Uid_usuario, sede_usuario AS Uid_sede, nombre_persona AS nombre, apellido_persona AS apellido FROM personas INNER JOIN usuarios ON persona_usuario = id_persona',  (error, results, fields) => {
                connection.query('SELECT id_persona AS Uid, tipo_tratamiento AS Uid_tipoTratamiento, nombre_tipoTratamiento AS tipoTratamiento, tratamiento_persona AS Uid_tratamiento, nombre_tratamiento AS tratamiento, departamento_municipio AS Uid_departamento, nombre_departamento AS departamento, municipioNacimiento_persona AS Uid_municipio, nombre_municipio AS municipio, nombre_persona AS nombre, apellido_persona AS apellido, identificacion_persona AS identificacion, DATE_FORMAT(fechaNacimiento_persona, "%Y-%m-%d") AS nacimiento, domicilio_persona AS direccion, telefono_persona AS telefono, correo_persona AS email, id_usuario AS Uid_usuario, grupo_usuario AS Uid_grupo, nombre_grupo AS grupo, cargo_usuario AS Uid_cargo, nombre_cargo AS cargo, empresa_sede AS Uid_empresa, nombre_empresa AS empresa, sede_usuario AS Uid_sede, nombre_sede AS sede, rol_usuario AS Uid_rol, nombre_rol AS rol, estado_usuario AS estado, foto_usuario AS avatar, nombre_usuario AS usuario, telefono_usuario AS telefonoC, correo_usuario AS emailC FROM personas INNER JOIN tratamientos ON tratamiento_persona = id_tratamiento INNER JOIN tipos_tratamientos ON id_tipoTratamiento = tipo_tratamiento LEFT JOIN municipios ON municipioNacimiento_persona = id_municipio LEFT JOIN departamentos ON departamento_municipio = id_departamento INNER JOIN usuarios ON id_persona = persona_usuario INNER JOIN grupos ON grupo_usuario = id_grupo INNER JOIN cargos ON cargo_usuario = id_cargo INNER JOIN sedes ON sede_usuario = id_sede INNER JOIN empresas ON empresa_sede = id_empresa INNER JOIN roles ON rol_usuario = id_rol WHERE superAdmin_usuario != 1', (error, results, fields) => {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    if (error) {
                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                        return connection.rollback(() => {
                            throw error; // Mensaje de error.
                        });
                    }
                    // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                    connection.commit((err) => {
                        if (err) {
                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                            return connection.rollback(() => {
                                throw err; // Mensaje de error.
                            });
                        }
                        resolve(results); //Resolver y devolver la promesa exitosa.
                    });
                });
            });
        });
    });
}

//Function para listar todas las personas
function listarPersona() {
    // Devolucion de una promesa.
    return new Promise((resolve, reject) => {
        // Abrimos una conexion a la base de datos.
        conn.getConnection((err, connection) => {
            // Inicio de transacción.
            connection.beginTransaction((err) => {
                if (err) {
                    throw err;
                } // Mensaje de error.
                connection.query('SELECT id_persona AS Uid, nombre_persona AS nombre, apellido_persona AS apellido FROM personas INNER JOIN usuarios ON persona_usuario = id_persona', (error, results, fields) => {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    if (error) {
                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                        return connection.rollback(() => {
                            throw error; // Mensaje de error.
                        });
                    }
                    // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                    connection.commit((err) => {
                        if (err) {
                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                            return connection.rollback(() => {
                                throw err; // Mensaje de error.
                            });
                        }
                        resolve(results); //Resolver y devolver la promesa exitosa.
                    });
                });
            });
        });
    });
}

function editarFoto(foto, usuario) {
    return new Promise((resolve, reject) => {
        imgResize.resize(foto, `Usuarios/${usuario}`, function (err) {
            if (err) {
                reject(err);
            }

            conn.getConnection((error, connection) => {
                if (error) {
                    throw error;
                }

                connection.beginTransaction((error) => {
                    if (error) {
                        throw error;
                    }
                    connection.query('SELECT foto_usuario AS foto FROM  usuarios WHERE nombre_usuario = ?', [usuario], (error, results, fields) => {

                        if (error) {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                            return connection.rollback(() => {
                                throw error; // Mensaje de error.
                            });
                        }
                        if (results[0].foto != '' && results[0].foto) {
                            const files = [
                                `./public/img/Usuarios/${usuario}/${results[0].foto}`,
                                `./public/img/Usuarios/${usuario}/.Metadatos/${results[0].foto}`,
                            ]

                            Promise.all(files.map(file => fs.unlink(file)))
                                .then(() => {
                                    console.log('All files removed')
                                })
                                .catch(err => {
                                    console.error('Something wrong happened removing files', err)
                                })
                        }
                        connection.query('UPDATE usuarios SET foto_usuario = ? WHERE nombre_usuario = ? ', [foto[0].filename, usuario], (error, results, fields) => {
                            connection.release();
                            if (error) {
                                return connection.rollback(() => {
                                    throw error;
                                })
                            }
                            connection.commit((error) => {
                                if (error) {
                                    return connection.rollback(() => {
                                        throw error;
                                    })
                                } else if (results.affectedRows) {
                                    resolve('exito');
                                } else {
                                    console.error('[Store marca] Algo salio mal al editar la foto');
                                    reject('Error al editar la foto algo salio mal');
                                    return false;
                                }
                            })
                        })
                    })
                })
            })
        })
    })
}

function editarPersona(usuario, file) {

    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                let name =""
                if (file) {
                    console.log("esxiste");
                    imgResize.resize3(file.image, `FirmaOne/${file.image.name}`, function (err) {
                      if (err) {
                          connection.release(); // Devolvemos la conexion/cerramos conexión.
                          return connection.rollback(() => {
                              throw err;
                          })
                          // An unknown error occurred when uploading.
                      }
                    })
                  name = file.image.name;
                }else{
                  name = ""
                }


                connection.query('UPDATE personas INNER JOIN usuarios ON id_persona = persona_usuario SET tratamiento_persona = ?, municipioNacimiento_persona = ?, nombre_persona = ?, apellido_persona = ?, identificacion_persona = ?, fechaNacimiento_persona = ?, domicilio_persona = ?, telefono_persona = ?, correo_persona = ?, grupo_usuario = ?, cargo_usuario = ?, sede_usuario = ?, rol_usuario = ?, estado_usuario = ?, nombre_usuario = ?, telefono_usuario = ?, correo_usuario = ?, img = ? WHERE id_persona = ?', [usuario.tratamiento, usuario.municipio, usuario.nombre, usuario.apellido, usuario.identificacion, usuario.nacimiento, usuario.direccion, usuario.telefono, usuario.email, usuario.grupo, usuario.cargo, usuario.sede, usuario.rol, usuario.estado, usuario.usuario, usuario.telefonoC, usuario.emailC, name , usuario.id], (error, results, fields) => {
                    if (error) {
                        connection.release();
                        return connection.rollback(() => {
                            throw error;
                        })
                    }
                    if ((usuario.contrasena != null) && (usuario.contrasena != '')) {
                        // Encriptación de contraseña (Fase1)
                        bcrypt.genSalt(saltRounds, function (erro, salt) {

                            if (erro) {
                                throw erro;
                            } // Mensaje de error.
                            // Encriptación de contraseña (Fase2)
                            bcrypt.hash(usuario.contrasena, salt, function (erre, hash) {
                                if (erre) {
                                    throw erre;
                                } // Mensaje de error.
                                connection.query('UPDATE personas INNER JOIN usuarios ON id_persona = persona_usuario SET contrasena_usuario = ? WHERE id_persona = ?', [hash, usuario.id], (errorr, results, fields) => {
                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                    if (errorr) {
                                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                        return connection.rollback(() => {
                                            throw errorr; // Mensaje de error.
                                        });
                                    }

                                    // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                                    connection.commit((errOm) => {
                                        if (errOm) {
                                            return connection.rollback(() => {
                                                throw errOm; // Mensaje de error.
                                            });
                                        }
                                        resolve('exito'); //Resolver y devolver la promesa exitosa.
                                    });
                                });
                            });
                        });
                    }
                    connection.commit((error) => {
                        if (error) {
                            return connection.rollback(() => {
                                throw error;
                            })
                        } else if (results.affectedRows) {
                            resolve('exito');
                        } else {
                            console.error('[Store marca] Algo salio mal al editar el estado');
                            reject('Error al editar el estado algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function editarPerfil(perfil) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('SELECT id_usuario AS id FROM  usuarios WHERE nombre_usuario = ?', [perfil.usuario], (error, results, fields) => {
                    if (error) {
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                        return connection.rollback(() => {
                            throw error; // Mensaje de error.
                        });
                    }
                    // results[0].id
                    connection.query('UPDATE personas INNER JOIN usuarios ON id_persona = persona_usuario SET  municipioNacimiento_persona = ?, nombre_persona = ?, apellido_persona = ?, identificacion_persona = ?, fechaNacimiento_persona = ?, domicilio_persona = ?, telefono_persona = ?, correo_persona = ? WHERE id_usuario = ?', [perfil.municipio, perfil.nombre, perfil.apellido, perfil.identificacion, perfil.nacimiento, perfil.direccion, perfil.telefono, perfil.email, results[0].id], (error, result, fields) => {
                        if (error) {
                            connection.release();
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        if ((perfil.contrasena != null) && (perfil.contrasena != '')) {

                            // Encriptación de contraseña (Fase1)
                            bcrypt.genSalt(saltRounds, function (erro, salt) {

                                if (erro) {
                                    throw erro;
                                } // Mensaje de error.
                                // Encriptación de contraseña (Fase2)
                                bcrypt.hash(perfil.contrasena, salt, function (erre, hash) {

                                    if (erre) {
                                        throw erre;
                                    } // Mensaje de error.
                                    connection.query('UPDATE usuarios SET contrasena_usuario = ? WHERE id_usuario = ?', [hash, results[0].id], (errorr, resul, fields) => {

                                        if (errorr) {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                            return connection.rollback(() => {
                                                throw errorr; // Mensaje de error.
                                            });
                                        }

                                        // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                                        connection.commit((errOm) => {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            if (errOm) {
                                                return connection.rollback(() => {
                                                    reject('Error cambiar la contraseña.');
                                                    throw errOm; // Mensaje de error.
                                                });
                                            }
                                            resolve('exito'); //Resolver y devolver la promesa exitosa.
                                        });
                                    });
                                });
                            });
                        }else{
                            connection.commit((error) => {
                                // connection.release(); // Devolvemos la conexion/cerramos conexión.
                                if (error) {
                                    return connection.rollback(() => {
                                        throw error;
                                    })
                                } else if (results.affectedRows) {
                                    resolve('exito');
                                } else {
                                    console.log(error);
                                    console.error('[Store marca] Algo salio mal al editar el perfil');
                                    reject('Error al editar el perfil algo salio mal');
                                    return false;
                                }
                            })
                        }
                    })
                })
            })
        })
    })
}

function editarEstado(usuario) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('UPDATE usuarios SET estado_usuario = ? WHERE id_usuario = ? ', [usuario.estado, usuario.id], (error, results, fields) => {
                    connection.release();
                    if (error) {
                        return connection.rollback(() => {
                            throw error;
                        })
                    }
                    connection.commit((error) => {
                        if (error) {
                            return connection.rollback(() => {
                                throw error;
                            })
                        } else if (results.affectedRows) {
                            resolve('exito');
                        } else {
                            console.error('[Store marca] Algo salio mal al editar el estado');
                            reject('Error al editar el estado algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

//Function para eliminar las personas
function delet(persona) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject) => {
        // Abrimos una conexion a la base de datos.
        conn.getConnection((errC, connection) => {
            if (errC) {
                throw errC;
            } // Mensaje de error.
            // Inicio de transacción.
            connection.beginTransaction(function (err) {
                if (err) {
                    throw err;
                } // Mensaje de error.
                connection.query('DELETE FROM persona WHERE id_persona = ?', [persona.uid], (error, results, fields) => {
                    if (error) {
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                        return connection.rollback(() => {
                            throw error; // Mensaje de error.
                        });
                    }
                    connection.query('DELETE FROM usuario WHERE persona_usuario = ?', [persona.uid], (errorr, results, fields) => {
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        if (errorr) {
                            //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                            return connection.rollback(() => {
                                throw errorr; // Mensaje de error.
                            });
                        }
                        // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                        connection.commit((errOm) => {
                            if (errOm) {
                                return connection.rollback(() => {
                                    throw errOm; // Mensaje de error.
                                });
                            }

                            if (results.affectedRows) {
                                resolve('exito'); //Resolver y devolver la promesa exitosa.
                            } else {
                                // Devolución de errores.
                                console.error('[MessageController] Persona no encontrada');
                                reject('Los datos son incorrectos2');
                                return false;
                            }
                        });
                    });
                });
            });
        });
    });
}


// Exportamos las funciones (modulos).
module.exports = {
    list,
    listarPersona,
    add,
    editarFoto,
    editarEstado,
    editarPersona,
    delet,
    editarPerfil
}
