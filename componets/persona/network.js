// Network de personas.
const express = require('express'); // Invocacion de Express.
// const jwt = require("jsonwebtoken"); // Invocación del sistema de Tokens.
const response = require('../../network/response'); //Invocaíon del response.
const {
    verifyToken,
    sAdmin,
    agregar,
    editar,
    eliminar
} = require('../../network/valiToken'); //Validacion del token.
const controller = require('./controller'); // Invocación del controlador.

const router = express.Router(); // Uso de rutas de express.
const multer = require('multer');
const imgResize = require('../../network/imgResize');
const fs = require('fs');
const fileupload = require("express-fileupload");

// Ruta para listar personas.
router.get('/', [verifyToken, sAdmin], (req, res) => {
    // Llamado del componente getPersona del controlador.
    controller.getPersona()
        .then((messageList) => {
            response.success(req, res, messageList, 200); // Mensaje de validacion de exito.
        })
        .catch(e => {
            response.error(req, res, 'Error inesperado', 500); // Mensaje de validacion de error.
        })
});
// Ruta para listar personas.
router.get('/persona', verifyToken, (req, res) => {
    // Llamado del componente getPersona del controlador.
    controller.getAllPersona()
        .then((messageList) => {
            response.success(req, res, messageList, 200); // Mensaje de validacion de exito.
        })
        .catch(e => {
            response.error(req, res, 'Error inesperado', 500); // Mensaje de validacion de error.
        })
});

router.post('/foto_perfil', verifyToken, (req, res) => {
    imgResize.upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            // A Multer error occurred when uploading.
        } else if (err) {
            // An unknown error occurred when uploading.
        }
        // console.log(req.files.image[0]);
        controller.editarFoto(req.files.image, req.body).then((result) => {
            response.success(req, res, result, 200);
        }).catch(e => {
            response.error(req, res, 'Error al agregar el item', 400, 'Porfavor verificar agregar item');
        })
    })
})

router.post('/perfil', verifyToken, (req, res) => {
    controller.editarPerfil(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al editar el perfil', 400, 'Porfavor verificar editar perfil');
    })
})

router.post('/estado/:Uid', [verifyToken, sAdmin, editar], (req, res) => {
    // Llamado del componente addPersona del controlador.
    controller.editarEstado(req.params.Uid, req.body.estado)
        .then((fullPersona) => {
            response.success(req, res, fullPersona, 201); // Mensaje de validacion de exito.
        })
        .catch(e => {
            response.error(req, res, 'Error informacion invalidad', 400, e); // Mensaje de validacion de error.
        });
});

router.post('/', [verifyToken, sAdmin, agregar], (req, res) => {
    // Llamado del componente addPersona del controlador.
    console.log(req.files);
    console.log(req.body);
    controller.addPersona(req.files, req.body)
        .then((fullPersona) => {
            response.success(req, res, fullPersona, 201); // Mensaje de validacion de exito.
        })
        .catch(e => {
            response.error(req, res, 'Error informacion invalidad', 400, e); // Mensaje de validacion de error.
        });

});

router.post('/:Uid', [verifyToken, sAdmin, editar], (req, res) => {
    controller.editarPersona(req.params.Uid, req.body, req.files)
        .then((fullPersona) => {
            response.success(req, res, fullPersona, 201); // Mensaje de validacion de exito.
        })
        .catch(e => {
            response.error(req, res, 'Error informacion invalidad', 400, e); // Mensaje de validacion de error.
        });
});

// Ruta para agregar persona/usuario.
router.delete('/:uid', [verifyToken, sAdmin, eliminar], (req, res) => {
    // Llamado del componente addPersona del controlador.
    controller.delPersona(req.params.Uid)
        .then((fullPersona) => {
            response.success(req, res, fullPersona, 201); // Mensaje de validacion de exito.
        })
        .catch(e => {
            response.error(req, res, 'Error informacion invalidad', 400, 'este mensaje es para desarrollador'); // Mensaje de validacion de error.
        });
});

// Exportamos las funciones (modulos).
module.exports = router;
