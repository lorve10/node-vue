// Controlador usuario.
const store = require('./store'); // Invocación del archivo store.

// Funcion para agregar persona/usuario.
function addPersona(file, persona) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        // Comprovación si no llega campos vacios.
        if (!persona.nombre || !persona.apellido || !persona.email || !persona.emailC || !persona.usuario || !persona.tratamiento || !persona.municipio || !persona.cargo || !persona.sede || !persona.rol || !persona.grupo || !persona.identificacion || !persona.nacimiento) {
            // Devolución de errores.
            console.error('[MessageController] No hay usuario o mensaje');
            reject('Los datos son incorrectos');
            return false;
        }
        if (!persona.contrasena) {
            persona.contrasena = "preventionw2021#Nfeoe";
            persona.contrasenaC = "preventionw2021#Nfeoe";
        }
        if(persona.contrasena != persona.contrasenaC){
            // Devolución de errores.
            console.error('[MessageController] Las contraseñas no coinsiden');
            reject('Las contraseñas no son iguales');
            return false;
        }
        // Llena array con datos enviados por la persona.
        const fullPersona = {
            'nombre': persona.nombre,
            'contrasena': persona.contrasena,
            'apellido': persona.apellido,
            'direccion': persona.direccion,
            'email': persona.email,
            'emailC': persona.emailC,
            'usuario': persona.usuario,
            'tratamiento': persona.tratamiento,
            'municipio': persona.municipio,
            'cargo': persona.cargo,
            'sede': persona.sede,
            'rol': persona.rol,
            'grupo': persona.grupo,
            'identificacion': persona.identificacion,
            'telefono': persona.telefono,
            'telefonoC': persona.telefonoC,
            'nacimiento': persona.nacimiento,
            'estado': persona.estadoU,
            'fecha': (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0,19)
        };
        resolve(store.add(file, fullPersona)); // Resolver y devolver la promesa exitosa.
    });
}

function editarFoto(foto, persona){
    return new Promise((resolve, reject)=>{
        if (!persona.usuario) {
            // Devolución de errores.
            console.error('[MessageController] No hay usuario');
            reject('Los datos son incorrectos');
            return false;
        }

        resolve(store.editarFoto(foto, persona.usuario));
    })

}

function editarPersona(id, persona, file) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        // Comprovación si no llega campos vacios.
        if (!persona.nombre || !persona.apellido || !persona.email || !persona.emailC || !persona.usuario || !persona.tratamiento || !persona.municipio || !persona.cargo || !persona.sede || !persona.rol || !persona.grupo || !persona.identificacion || !persona.nacimiento) {
            // Devolución de errores.
            console.error('[MessageController] No hay usuario o mensaje');
            reject('Los datos son incorrectos');
            return false;
        }

        if(persona.contrasena != persona.contrasenaC){
            // Devolución de errores.
            console.error('[MessageController] Las contraseñas no coinsiden');
            reject('Las contraseñas no son iguales');
            return false;
        }

        // Llena array con datos enviados por la persona.
        const fullPersona = {
            id,
            'nombre': persona.nombre,
            'contrasena': persona.contrasena,
            'apellido': persona.apellido,
            'direccion': persona.direccion,
            'email': persona.email,
            'emailC': persona.emailC,
            'usuario': persona.usuario,
            'tratamiento': persona.tratamiento,
            'municipio': persona.municipio,
            'cargo': persona.cargo,
            'sede': persona.sede,
            'rol': persona.rol,
            'grupo': persona.grupo,
            'identificacion': persona.identificacion,
            'telefono': persona.telefono,
            'telefonoC': persona.telefonoC,
            'nacimiento': persona.nacimiento,
            'estado': persona.estadoU,
            'fecha': (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0,19)
        };

        // if (persona.contrasena != null) {
        //     Object.assign({'contrasena': persona.contrasena}, fullPersona);
        // }

        resolve(store.editarPersona(fullPersona, file)); // Resolver y devolver la promesa exitosa.
    });
}

function editarPerfil(perfil) {
    // // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        // Comprovación si no llega campos vacios.
        // console.log(perfil.identificacion, perfil.nombre , perfil.apellido , perfil.nacimiento ,perfil.depatamento, perfil.municipio);
        if (!perfil.identificacion || !perfil.nombre || !perfil.apellido || !perfil.nacimiento || !perfil.departamento || !perfil.municipio) {
            // Devolución de errores.
            console.error('[MessageController] Los datos no estan completos.');
            reject('Los datos son incorrectos');
            return false;
        }

        if(perfil.contrasena != perfil.contrasenaC){
            // Devolución de errores.
            console.error('[MessageController] Las contraseñas no coinsiden');
            reject('Las contraseñas no son iguales');
            return false;
        }

        // Llena array con datos enviados por la persona.
        const fullPerfil = {
            'contrasena': perfil.contrasena,
            'nombre': perfil.nombre,
            'apellido': perfil.apellido,
            'direccion': perfil.direccion,
            'email': perfil.email,
            'usuario': perfil.usuario,
            'municipio': perfil.municipio,
            'departamento': perfil.departamento,
            'identificacion': perfil.identificacion,
            'telefono': perfil.telefono,
            'nacimiento': perfil.nacimiento,
            'fecha': (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0,19)
        };

        resolve(store.editarPerfil(fullPerfil)); // Resolver y devolver la promesa exitosa.
    });
}

// Funcion para agregar persona/usuario.
function editarEstado(id, estado) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        // Comprovación si no llega campos vacios.
        if ( !id || estado<0 || estado>1) {
            // Devolución de errores.
            console.error('[MessageController] No hay usuario o mensaje');
            reject('Los datos son incorrectos');
            return false;
        }
        // Llena array con datos enviados por la persona.
        const fullPersona = {
            id:id,
            estado:estado,
        };
        resolve(store.editarEstado(fullPersona)); // Resolver y devolver la promesa exitosa.
    });
}

// Funcion para listar los usuarios
function getPersona() {
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        resolve(store.list()); // Resolver y devolver la promesa exitosa.
    });
}
// Funcion para listar los usuarios
function getAllPersona() {
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        resolve(store.listarPersona()); // Resolver y devolver la promesa exitosa.
    });
}

// Funcion para agregar persona/usuario.
function delPersona(uid) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        // Comprovación si no llega campos vacios.
        if (!uid) {
            // Devolución de errores.
            console.error('[MessageController] No hay uid');
            reject('Dato es incorrecto');
            return false;
        }
        // Llena array con datos enviados por la persona.
        const fullPersona = {
            uid:uid,
        };
        resolve(store.delet(fullPersona)); // Resolver y devolver la promesa exitosa.
    });
}

// Exportamos las funciones (modulos).
module.exports = {
    addPersona,
    getAllPersona,
    editarFoto,
    editarPersona,
    editarPerfil,
    editarEstado,
    getPersona,
    delPersona
}
