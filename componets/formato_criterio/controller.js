const store = require('./store');

function listarFormCrit(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarFormCrit());
    })
}

function agregarFormCrit(criterio) {
    return new Promise((resolve, reject)=>{
        if (!criterio.formatoCriterio || !criterio.descripcionCriterio) {
            console.error('[controller formato_criterio] Algun campo llego vacio');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullFormCrit = {
            formato : criterio.formatoCriterio,
            descripcion : criterio.descripcionCriterio
        }

        resolve(store.agregarFormCrit(fullFormCrit));
    })
}

function actualizarFormCrit(id, criterio) {
    return new Promise((resolve, reject) => {
        if(!id || !criterio.formatoCriterio || !criterio.descripcionCriterio){
            console.error('[controller formato_criterio] Algun campo llego vacio');
            reject('Algun campo llego vacio');
            return false;
        }

        const fullFormCrit = {
            id,
            formato : criterio.formatoCriterio,
            descripcion : criterio.descripcionCriterio
        }

        resolve(store.actualizarFormCrit(fullFormCrit));
    })
}

function eliminarFormCrit(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[controller formato_criterio] El campo llego vacio');
            reject('El campo llego vacio');
            return false;
        }
        resolve(store.eliminarFormCrit(id));
    })
}

module.exports = {
    listarFormCrit,
    agregarFormCrit,
    actualizarFormCrit,
    eliminarFormCrit
}