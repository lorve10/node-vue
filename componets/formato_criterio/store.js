const {conn} = require('../../settings/db');

function listarFormCrit() {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error}
                connection.query('SELECT id_formatoCriterio AS Uid, formato_formatoCriterio AS formato, descripcion_formatoCriterio AS descripcion FROM formatos_criterios', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarFormCrit(criterio){
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error;}
                connection.query('INSERT INTO formatos_criterios (formato_formatoCriterio, descripcion_formatoCriterio) VALUES (?,?)',[criterio.formato, criterio.descripcion], (error, results, fields)=>{
                    connection.release();
                    if (error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                    });
                })
            })
        })
    })
}

function actualizarFormCrit(criterio){
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error;}
                connection.query('UPDATE formatos_criterios SET formato_formatoCriterio = ?, descripcion_formatoCriterio = ? WHERE id_formatoCriterio = ?',[criterio.formato, criterio.descripcion, criterio.id], (error, results, fields)=>{
                    connection.release();
                    if (error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if(results.affectedRows){
                            resolve('exito')
                        }
                        else{
                            console.error('[store formato_criterio] algo ocurrio al momento de actualizar el criterio');
                            reject('Error al momento de actualizar el criterio');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

function eliminarFormCrit(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error}
                connection.query('DELETE FROM formatos_criterios WHERE id_formatoCriterio = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if(results.affectedRows){
                            resolve('exito')
                        }
                        else{
                            console.error('[store formato_criterio] algo ocurrio al momento de eliminar el criterio');
                            reject('Error al momento de eliminar el criterio');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

module.exports = {
    listarFormCrit,
    agregarFormCrit,
    actualizarFormCrit,
    eliminarFormCrit
}