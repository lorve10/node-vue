const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarFormCrit().then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al tratar de listar los Criterios', 500)
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarFormCrit(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al agregar el criterio', 400)
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarFormCrit(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el criterio', 400)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarFormCrit(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al eliminar el criterio', 400)
    })
})

module.exports = router;