const {conn} = require('../../settings/db'); 

function listarEstadoRemision(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                
                connection.query('SELECT id_estadoRemision AS Uid, alias_estadoRemision AS alias, nombre_estadoRemision AS nombre FROM estados_remisiones', (error, results, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarEstadoRemision(estadoRemision){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('INSERT INTO estados_remisiones (alias_estadoRemision, nombre_estadoRemision) VALUES (?, ?)',[estadoRemision.alias, estadoRemision.nombre], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarEstadoRemision(estadoRemision){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE estados_remisiones SET alias_estadoRemision = ?, nombre_estadoRemision = ? WHERE id_estadoRemision = ? ', [estadoRemision.alias, estadoRemision.nombre, estadoRemision.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store EstadoRemision] Algo salio mal al elimar el estado');
                            reject('Error al elimar el estado algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarEstadoRemision(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM estados_remisiones WHERE id_estadoRemision = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store EstadoRemision] Algo ocurrio al eliminar el estado');
                            reject('Fallo la eliminacion del estado');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarEstadoRemision,
    agregarEstadoRemision,
    actualizarEstadoRemision,
    eliminarEstadoRemision
}