const store = require('./store');

function listarEstadoRemision(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarEstadoRemision());
    })
}

function agregarEstadoRemision(estado){
    return new Promise((resolve, reject)=>{
        if(!estado.alias || !estado.nombre){
            console.error('[Controller EstadoRemision] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullEstadoRemision = {
            alias : estado.alias,
            nombre : estado.nombre
        }
        resolve(store.agregarEstadoRemision(fullEstadoRemision));
    }) 
}

function actualizarEstadoRemision(id, estado){
    return new Promise((resolve, reject)=>{
        if(!id || !estado.alias || !estado.nombre){
            console.error('[Controller EstadoRemision] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullEstadoRemision = {
            id, 
            alias : estado.alias,
            nombre : estado.nombre
        }
        resolve(store.actualizarEstadoRemision(fullEstadoRemision));
    })
}

function eliminarEstadoRemision(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller EstadoRemision] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarEstadoRemision(id));
    })
}


module.exports ={
    listarEstadoRemision,
    agregarEstadoRemision,
    actualizarEstadoRemision,
    eliminarEstadoRemision
}