const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarEstadoRemision().then((allEstadoRemision)=>{
        response.success(req, res, allEstadoRemision, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar los estado', 500, 'La consulta los estado salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarEstadoRemision(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el estado', 400, 'Porfavor verificar agregar estado');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarEstadoRemision(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el estado', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarEstadoRemision(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el estado',500,'algo ocurrio al eliminar el estado')
    })
})

module.exports = router;