const store = require('./store');

function listarMunicipio(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarMunicipio());
    })
}
function agregarMunicipio(municipio){
    return new Promise((resolve, reject)=>{
        if(!municipio.departamentoMunicipio || !municipio.nombreMunicipio){
            console.error('[Controller Municipio] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullMunicipio = {
            departamento : municipio.departamentoMunicipio,
            nombre : municipio.nombreMunicipio
        }
        resolve(store.agregarMunicipio(fullMunicipio));
    })
}
function actualizarMunicipio(id, municipio) {
    return new Promise((resolve, reject)=>{
        if (!id || !municipio.departamentoMunicipio || !municipio.nombreMunicipio) {
            console.error('[Controller Municipio] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullMunicipio = {
            id, 
            departamento : municipio.departamentoMunicipio,
            nombre : municipio.nombreMunicipio
        }
        resolve(store.actualizarMunicipio(fullMunicipio))
    })
}
function eliminarMunicipio(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Municipio] la informacion no llego');
            reject('Error al inrtentar eiminar');
            return false;
        }
        resolve (store.eliminarMunicipio(id));
    })
}

module.exports = {
    listarMunicipio,
    agregarMunicipio,
    actualizarMunicipio,
    eliminarMunicipio
}