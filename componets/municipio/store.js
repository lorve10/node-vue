const {conn} = require('../../settings/db')

function listarMunicipio(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_municipio AS Uid, departamento_municipio AS departamento, nombre_municipio AS nombre FROM municipios',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarMunicipio(municipio){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO municipios (departamento_municipio, nombre_municipio) VALUES (?, ?)', [municipio.departamento, municipio.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarMunicipio(municipio){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error) {throw error}
                connection.query('UPDATE municipios SET departamento_municipio = ?, nombre_municipio = ? WHERE id_municipio = ?', [municipio.departamento, municipio.nombre, municipio.id,], (error, results, fields)=>{
                    connection.release();
                    if (error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito');
                        }
                        else{
                            console.error('[store Municipio] algo ocurrio al actualizar el municipio');
                            reject('Error al actualizar el municipio');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarMunicipio(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM municipios WHERE id_municipio = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito')
                        }
                        else{
                            console.error('[store Municipio] Algo ocurrio al eliminar el municipio');
                            reject('Fallo la eliminacion del municipio');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarMunicipio,
    agregarMunicipio,
    actualizarMunicipio,
    eliminarMunicipio
}