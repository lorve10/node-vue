const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarMunicipio().then((allMunicipio)=>{
        response.success(req, res, allMunicipio, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar a los municipio', 500, 'La consulta a los municipio salio mal');
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarMunicipio(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al agregar el municipio', 400, 'Error al agregar el municipio');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarMunicipio(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al intentar actualizar el municipio', 400, 'Error al actualizar el municipio');
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarMunicipio(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo fallo al eliminar el municipio', 400, 'Error al eliminar el municipio');
    })
})

module.exports=router;