const {
    conn
} = require('../../settings/db');
const imgResize = require('../../network/imgResize');
const fs = require('fs').promises;
const fss = require('fs');
const {
    TemplateHandler,
    MimeType
} = require('easy-template-x');

function agregarCerti(item){
  return new Promise((resolve, reject)=>{
    conn.getConnection((error, connection)=>{
    if (error) {
      throw error;
    }
    connection.beginTransaction((error)=>{
      if(error){
        throw error;
      }
      connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [item.usuario], (error, rest, fields) => {
          if (error) {
              connection.release(); // Devolvemos la conexion/cerramos conexión.
              return connection.rollback(() => {
                  throw error;
              })
          }

            let base64Data = item.firma.replace(/^data:image\/png;base64,/,""),
            binaryData = new Buffer.from(base64Data, 'base64').toString('binary');
            require("fs").writeFile(`./public/formatos/Certificados/Firmas/${item.codigo}.png`, binaryData, "binary", function(err) { console.log(err)  })
            const firma = `${item.codigo}.png`;
          connection.query('INSERT INTO certificado_if(codigo, equipo, serial, fechaFabri, marca, cumple, fechaInsp, validez, itemId, userId, firma  ) VALUES (?,?,?,?,?,?,?,?,?,?,?)',[item.codigo, item.equipo, item.serial, item.fechaFabri, item.marca, item.cumple, item.fechaInspe, item.validez, item.itemId, rest[0].Uid, firma  ], (error, rest, fields) =>{
            if (error) {
                connection.release(); // Devolvemos la conexion/cerramos conexión.
                return connection.rollback(() => {
                    throw error;
                })
            }
            // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
            connection.commit((error) => {
                connection.release(); // Devolvemos la conexion/cerramos conexión.
                if (error) {
                    return connection.rollback(() => {
                        throw error; // Mensaje de error.
                    });
                }
                resolve('exito'); //Resolver y devolver la promesa exitosa.
            });
          })

      })

    })
  })
})
};

function cargar(item){
        return new Promise((resolve, reject)=>{
          conn.getConnection((error, connection)=>{
          if (error) {
            throw error;
          }
          connection.beginTransaction((error)=>{
              if(error){
                throw error;
              }
              connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [item.user], (error, rest, fields) => {
                  if (error) {
                      connection.release(); // Devolvemos la conexion/cerramos conexión.
                      return connection.rollback(() => {
                          throw error;
                      })
                  }

                  connection.query('SELECT * FROM  certificado_if WHERE itemId = ?', [item.numero],(error, results, fields) => {
                      connection.release();
                      if (error) {
                          return connection.rollback(() => {
                              throw error;
                          });
                      }
                      connection.commit((error) => {
                          if (error) {
                              return connection.rollback(() => {
                                  throw error;
                              })
                          }
                          resolve(results);
                      })
                  })

              })
            })


        })
      })
}


module.exports = {
  agregarCerti,
  cargar
}
