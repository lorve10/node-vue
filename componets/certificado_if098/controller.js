const store = require('./store');

function agregarCertificado(item){
  return new Promise((resolve, reject)=>{
      resolve(store.agregarCerti(item));
      })
}

function cargarCertificado(item){
    return new Promise((resolve, reject)=>{
            resolve(store.cargar(item));
    })
}

module.exports={
  agregarCertificado,
  cargarCertificado
}
