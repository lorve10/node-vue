const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const multer = require('multer');
const imgResize = require('../../network/imgResize');
const Excel = require("exceljs");
var mammoth = require("mammoth");
var pdf = require('html-pdf');
const { TemplateHandler, MimeType } = require('easy-template-x');
const { verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.
const fs = require('fs');
const fileupload = require("express-fileupload");

router.post('/', [verifyToken, agregar],(req, res) => {
  // if(req.body.firma !=null){
  //   base64Data = req.body.firma.replace(/^data:image\/png;base64,/,""),
  //   binaryData = new Buffer.from(base64Data, 'base64').toString('binary');
  //
  //
  //   require("fs").writeFile("cesar.png", binaryData, "binary", function(err) { console.log(err)  })
  controller.agregarCertificado(req.body).then((result) => {
      response.success(req, res, result, 200);
  }).catch(e => {
      response.error(req, res, 'Error al agregar el item', 400, 'Porfavor verificar agregar item');
  })
  //}
})


router.post('/getCertificado/', verifyToken, (req, res) => {
    controller.cargarCertificado(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al cargar inpeccion', 500, 'algo ocurrio')
    })
})

module.exports = router;
