const {conn} = require('../../settings/db'); 

function listarTipoEquipo(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                
                connection.query('SELECT id_tipoEquipo AS Uid, nombre_tipoEquipo AS nombre FROM tipos_equipos', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarTipoEquipo(tipoEquipo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('INSERT INTO tipos_equipos (nombre_tipoEquipo) VALUES (?)',[tipoEquipo.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarTipoEquipo(tipoEquipo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE tipos_equipos SET nombre_tipoEquipo = ? WHERE id_tipoEquipo = ? ', [tipoEquipo.nombre, tipoEquipo.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store Tipo] Algo salio mal al elimar el tipo');
                            reject('Error al elimar el tipo algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarTipoEquipo(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM tipos_equipos WHERE id_tipoEquipo = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store Tipo] Algo ocurrio al eliminar el tipo');
                            reject('Fallo la eliminacion del tipo');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarTipoEquipo,
    agregarTipoEquipo,
    actualizarTipoEquipo,
    eliminarTipoEquipo
}