const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarTipoEquipo().then((allTipoEquipo)=>{
        response.success(req, res, allTipoEquipo, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar los tipo', 500, 'La consulta los tipo salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarTipoEquipo(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el tipo', 400, 'Porfavor verificar agregar tipo');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarTipoEquipo(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el tipo', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarTipoEquipo(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el tipo',500,'algo ocurrio al eliminar el tipo')
    })
})

module.exports = router;