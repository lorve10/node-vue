const store = require('./store');

function listarTipoEquipo(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarTipoEquipo());
    })
}

function agregarTipoEquipo(tipo){
    return new Promise((resolve, reject)=>{
        if(!tipo.nombre){
            console.error('[Controller TipoEquipo] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullTipoEquipo = {
            nombre : tipo.nombre
        }
        resolve(store.agregarTipoEquipo(fullTipoEquipo));
    }) 
}

function actualizarTipoEquipo(id, tipo){
    return new Promise((resolve, reject)=>{
        if(!id || !tipo.nombre){
            console.error('[Controller TipoEquipo] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullTipoEquipo = {
            id,
            nombre : tipo.nombre
        }
        resolve(store.actualizarTipoEquipo(fullTipoEquipo));
    })
}

function eliminarTipoEquipo(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller TipoEquipo] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarTipoEquipo(id));
    })
}

module.exports ={
    listarTipoEquipo,
    agregarTipoEquipo,
    actualizarTipoEquipo,
    eliminarTipoEquipo
}