const store = require('./store');

function listarAsignacion(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarAsignacion());
    })
}

function agregarAsignacion(asignacion){
    return new Promise((resolve, reject)=>{
        if(asignacion.length <= 0){
            console.error('[Controller Item] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        
        const fullAsignacion = asignacion
        resolve(store.agregarAsignacion(fullAsignacion));
    }) 
    
}

function agregarDesAsignacion(asignacion){
    return new Promise((resolve, reject)=>{
        if(asignacion.length <= 0){
            console.error('[Controller Item] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        
        const fullDesAsignacion = asignacion
        resolve(store.agregarDesAsignacion(fullDesAsignacion));
    }) 
}

function formato(item){
    return new Promise((resolve, reject)=>{
        if(item == null){
            console.error('[Controller archivo] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.formato(item));    
    })
}

module.exports ={
    listarAsignacion,
    agregarAsignacion,
    agregarDesAsignacion,
    formato,
}