const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {
    verifyToken,
    agregar,
    editar,
    eliminar
} = require('../../network/valiToken'); //Validacion del token.


router.get('/', verifyToken, (req, res) => {
    controller.listarAsignacion().then((allAsignacion) => {
        response.success(req, res, allAsignacion, 200);
    }).catch(e => {
        response.error(req, res, 'No se pudo listar las asignaciones', 500, 'La consulta a las asignaciones salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res) => {
    controller.agregarAsignacion(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al agregar la asignacion', 400, 'Porfavor verificar agregar la asignacion');
    })
})

router.post('/desasignacion', [verifyToken, agregar], (req, res) => {
    controller.agregarDesAsignacion(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al agregar la asignacion', 400, 'Porfavor verificar agregar la asignacion');
    })
})

router.post('/formato', [verifyToken, agregar], (req, res) => {
    controller.formato(req.body).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al agregar la asignacion', 400, 'Porfavor verificar agregar la asignacion');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res) => {
    controller.actualizarAsignacion(req.params.Uid, req.body).then((result) => {
        response.success(req, res, result, 200)
    }).catch(e => {
        response.error(req, res, 'Algo salio mal al actualizar la asignacion', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res) => {
    controller.eliminarAsignacion(req.params.Uid).then((result) => {
        response.success(req, res, result, 200);
    }).catch(e => {
        response.error(req, res, 'Error al eliminar la asignacion', 500, 'algo ocurrio al eliminar la asignacion')
    })
})

module.exports = router;