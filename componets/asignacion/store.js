const {
    conn
} = require('../../settings/db');
const fs = require('fs');
const {
    TemplateHandler
} = require('easy-template-x');

function listarAsignacion() {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {

            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('SELECT id_asignacion AS desasignacion, descripcion_asignacion AS notaDesasign, DATE_FORMAT(fecha_asignacion,"%Y-%m-%d") AS fechaDesasig, id_item AS Uid, item_asignacion AS item, usuario_asigna AS asignador, usuario_asignacion AS asignado, descripcion_asignacion AS descripcion, fecha_asignacion AS fecha, tipoItem_item AS Uid_tipoItem, estado_item AS Uid_estado, archivo_imagenItem AS foto, COUNT(item_imagenItem) cantidad, asignacion_item AS asignacion, codigo_item AS codigo, formato_item AS Uid_formato, bodega_item AS Uid_bodega, nombre_bodega AS bodega, tipoEquipo_item AS Uid_tipoEquipo, descripcion_item AS descripcion, marca_item AS Uid_marca, nombre_marca AS marca, fabricante_marca AS Uid_fabricante, proveedor_item AS Uid_proveedor, modelo_item AS modelo, serial_item AS serial, vidaUtil_item AS vidaUtil, sede_bodega AS Uid_sede, nombre_sede AS sede, DATE_FORMAT(fechaFabricacion_item,"%Y-%m-%d") AS fechaFabricacion, DATE_FORMAT(fechaPuestaUso_item,"%Y-%m-%d") AS fechaUso, uso_item AS Uid_uso, otroUso_item AS otroUso, nombreKit_item AS kit, informacionKit_item AS informacionKit, cumpleNormas_item AS cumpleNorma, otrasEspecificacionesTecnicas_item AS especificaciones, valor_item AS valor, numeroFactura_item AS factura, capacidad_item AS capacidad, lote_item AS lote, DATE_FORMAT(fechaCompra_item,"%Y-%m-%d") AS fechaCompra, DATE_FORMAT(fechaVencimiento_item,"%Y-%m-%d") AS vencimiento, manualFabricante_item AS manual, certificacionFabricante_item AS certificacion, usuario_item AS Uid_usuario, persona_usuario AS Uid_persona, identificacion_persona AS identificacion, nombre_persona AS nombre, apellido_persona AS apellido, nombre_cargo AS cargo, count(codigo_item) cantidad FROM  items_asignaciones INNER JOIN items ON id_item = item_asignacion INNER JOIN bodegas ON bodega_item = id_bodega INNER JOIN sedes ON id_sede = sede_bodega INNER JOIN proveedores ON proveedor_item = id_proveedor INNER JOIN marcas ON marca_item = id_marca INNER JOIN usuarios ON usuario_item = id_usuario INNER JOIN cargos ON cargo_usuario = id_cargo INNER JOIN personas ON persona_usuario = id_persona INNER JOIN tipos_items ON tipoItem_item = id_tipoItem INNER JOIN estados_items ON estado_item = id_estadoItem INNER JOIN usos_items ON uso_item = id_usoItem LEFT JOIN imagenes_items ON id_item = item_imagenItem WHERE asignacion_item = 1 group by codigo_item ORDER BY fechaVencimiento_item DESC', (error, results, fields) => {

                    if (error) {
                        return connection.rollback(() => {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        });
                    }
                    connection.commit((error) => {
                        if (error) {
                            return connection.rollback(() => {
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarAsignacion(asignacion) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [asignacion[0].asigna], (error, rest, fields) => {
                    if (error) {
                        return connection.rollback(() => {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    const fecha = new Date().toISOString().substr(0, 10);
                    asignacion.forEach((element, index) => {
                        connection.query('SELECT id_asignacion FROM items_asignaciones INNER JOIN items ON (item_asignacion = id_item) AND (asignacion_item = 0) WHERE id_item = ?', [element.id], (error, restExis, fields) => {
                            if (error) {
                                return connection.rollback(() => {
                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                    throw error;
                                })
                            }
                            if (restExis.length > 0) {
                                connection.query('DELETE FROM items_asignaciones WHERE id_asignacion = ?', [restExis[0].id_asignacion], (error, results, fields) => {

                                    if (error) {
                                        return connection.rollback(() => {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            throw error;
                                        })
                                    }
                                    connection.commit((error) => {
                                        if (error) {
                                            return connection.rollback(() => {
                                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                throw error;
                                            })
                                        }
                                    });
                                });
                            }
                            connection.query('INSERT INTO items_asignaciones (item_asignacion, usuario_asigna, usuario_asignacion, descripcion_asignacion, fecha_asignacion, estado_asignacion) VALUES (?,?,?,?,?,?)', [element.id, rest[0].Uid, element.usuario, element.value, fecha, 1], (errorAsign, results, fields) => {
                                if (errorAsign) {
                                    //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                    return connection.rollback(() => {
                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                        throw errorAsign; // Mensaje de error.
                                    });

                                }
                                connection.query('INSERT INTO items_asignaciones_log (item_asignacion, usuario_asigna, usuario_asignacion, descripcion_asignacion, fecha_asignacion, estado_asignacion) VALUES (?,?,?,?,?,?)', [element.id, rest[0].Uid, element.usuario, element.value, fecha, 1], (errorLog, results, fields) => {
                                    if (errorLog) {
                                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                        return connection.rollback(() => {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            throw errorLog; // Mensaje de error.
                                        });
                                    }
                                    connection.query('UPDATE items SET asignacion_item = ? WHERE id_item = ? ', [1, element.id], (errorUp, resultsUp, fields) => {
                                        if (errorUp) {
                                            return connection.rollback(() => {
                                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                throw errorUp;
                                            })
                                        }
                                        if ((index + 1) == asignacion.length) {
                                            connection.commit((errOm) => {
                                                if (errOm) {
                                                    return connection.rollback(() => {
                                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                        throw errOm; // Mensaje de error.
                                                    });
                                                } else if (resultsUp.affectedRows) {
                                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                    resolve('exito');
                                                } else {
                                                    return connection.rollback(() => {
                                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                        console.error('[Store item] Algo salio mal al asignar el item');
                                                        reject('Error al asignat el item algo salio mal');
                                                    });
                                                }
                                            });
                                        }
                                    })
                                })
                            })
                        })
                    });
                })
            })
        })
    })
}

function agregarDesAsignacion(asignacion) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('SELECT id_usuario AS Uid FROM usuarios WHERE nombre_usuario = ?', [asignacion[0].asigna], (error, rest, fields) => {
                    if (error) {
                        return connection.rollback(() => {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    const fecha = new Date().toISOString().substr(0, 10);
                    asignacion.forEach((element, index) => {
                        connection.query('SELECT id_asignacion AS idAsig FROM items_asignaciones INNER JOIN items ON item_asignacion = id_item WHERE (item_asignacion = ?) AND (asignacion_item = 1)', [element.id], (error, resutDes, fields) => {
                            if (error) {
                                return connection.rollback(() => {
                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                    throw error;
                                })
                            }
                            connection.query('UPDATE items_asignaciones SET usuario_desasigna = ?, usuario_desasignacion = ?, descripcion_desasignacion = ?, fecha_desasignacion = ?, estado_asignacion = ? WHERE id_asignacion = ?', [rest[0].Uid, element.usuario, element.value, fecha, 0, resutDes[0].idAsig], (errorDesAsign, resultsDes, fields) => {
                                if (errorDesAsign) {
                                    //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                    return connection.rollback(() => {
                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                        throw errorDesAsign; // Mensaje de error.
                                    });
                                }
                                connection.query('INSERT INTO items_asignaciones_log (item_asignacion, usuario_asigna, usuario_asignacion, descripcion_asignacion, fecha_asignacion, estado_asignacion) VALUES (?,?,?,?,?,?)', [element.id, rest[0].Uid, element.usuario, element.value, fecha, 0], (errorLog, results, fields) => {
                                    if (errorLog) {
                                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                        return connection.rollback(() => {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            throw errorLog; // Mensaje de error.
                                        });
                                    }
                                    connection.query('UPDATE items SET asignacion_item = ? WHERE id_item = ? ', [0, element.id], (errorUp, resultsUp, fields) => {
                                        if (errorUp) {
                                            return connection.rollback(() => {
                                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                throw errorUp;
                                            })
                                        }
                                        if ((index + 1) == asignacion.length) {
                                            connection.commit((errOm) => {
                                                if (errOm) {
                                                    return connection.rollback(() => {
                                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                        throw errOm; // Mensaje de error.
                                                    });
                                                } else if (resultsUp.affectedRows) {
                                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                    resolve('exito');
                                                } else {
                                                    return connection.rollback(() => {
                                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                                        console.error('[Store item] Algo salio mal al asignar el item');
                                                        reject('Error al asignat el item algo salio mal');
                                                    });
                                                }
                                            });
                                        }
                                    })
                                })
                            })
                        })
                    });
                })
            })
        })
    })
}

function formato(item) {
    return new Promise(async (resolve, reject) => {
        // 1. read template file
        templateFile = fs.readFileSync('./public/formatos/Asignacion.docx');
        const lista = [];
        let i = 0;
        item.forEach((element, index) => {
            i = ++index;
            if (item.length != index) {
                lista.push({
                    'numero': i,
                    'descripcion': element.descripcion,
                    'cantidad': element.cantidad,
                    'codigo': element.codigo,
                    'referencia': element.serial
                })
            }
            
        });
        // 2. process the template
        const data = {
            'lugar': item[0].sede,
            'nombreEntrega': `${item[(item.length)-1].nombre} ${item[(item.length)-1].apellido}`,
            'identificacionEntrega': item[(item.length)-1].identificacion,
            'cargoEntrega': item[(item.length)-1].cargo,
            'nombreRecibe': `${item[0].nombre} ${item[0].apellido}`,
            'identificacionRecibe': item[0].identificacion,
            'cargoRecibe': item[0].cargo,
            lista
        };

        const handler = new TemplateHandler();
        const doc = await handler.process(templateFile, data);
        fs.writeFile(`./public/formatos/myTemplate - output.docx`, doc, (err) => {
            if (err) {
                console.log(err);
            }
            console.log(item);
            resolve('myTemplate - output.docx')
            
        });

    })
}

module.exports = {
    listarAsignacion,
    agregarAsignacion,
    agregarDesAsignacion,
    formato
}