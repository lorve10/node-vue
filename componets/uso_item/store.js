const {conn} = require('../../settings/db'); 

function listarUsoItem(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                
                connection.query('SELECT id_usoItem AS Uid, alias_usoitem AS alias, nombre_usoItem AS nombre FROM usos_items', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarUsoItem(usoItem){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('INSERT INTO usos_items (alias_usoItem, nombre_usoItem) VALUES (?, ?)',[usoItem.alias, usoItem.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarUsoItem(usoItem){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE usos_items SET alias_usoItem = ?, nombre_usoItem = ? WHERE id_usoItem = ? ', [usoItem.alias, usoItem.nombre, usoItem.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store Uso] Algo salio mal al elimar el uso');
                            reject('Error al elimar el uso algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarUsoItem(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM usos_items WHERE id_usoItem = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store Uso] Algo ocurrio al eliminar el uso');
                            reject('Fallo la eliminacion del uso');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarUsoItem,
    agregarUsoItem,
    actualizarUsoItem,
    eliminarUsoItem
}