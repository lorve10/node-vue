const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarUsoItem().then((allUsoItem)=>{
        response.success(req, res, allUsoItem, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar los usos', 500, 'La consulta los usos salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarUsoItem(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el usos', 400, 'Porfavor verificar agregar usos');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarUsoItem(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el usos', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarUsoItem(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el usos',500,'algo ocurrio al eliminar el usos')
    })
})

module.exports = router;