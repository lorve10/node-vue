const store = require('./store');

function listarUsoItem(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarUsoItem());
    })
}

function agregarUsoItem(uso){
    return new Promise((resolve, reject)=>{
        if(!uso.alias || !uso.nombre){
            console.error('[Controller UsoItem] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullUsoItem = {
            alias : uso.alias, 
            nombre : uso.nombre
        }
        resolve(store.agregarUsoItem(fullUsoItem));
    }) 
}

function actualizarUsoItem(id, uso){
    return new Promise((resolve, reject)=>{
        if(!id || !uso.alias || !uso.nombre){
            console.error('[Controller UsoItem] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullUsoItem = {
            id,
            alias : uso.alias, 
            nombre : uso.nombre
        }
        resolve(store.actualizarUsoItem(fullUsoItem));
    })
}

function eliminarUsoItem(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller UsoItem] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarUsoItem(id));
    })
}

module.exports ={
    listarUsoItem,
    agregarUsoItem,
    actualizarUsoItem,
    eliminarUsoItem
}