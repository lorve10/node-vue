const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarDepartamento().then((allDepartamento)=>{
        response.success(req, res, allDepartamento, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar a los departamentos', 500, 'La consulta a los departamentos salio mal');
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarDepartamento(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al agregar el departamento', 400, 'Error al agregar el departamento');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarDepartamento(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al intentar actualizar el departamento', 400, 'Error al actualizar el departamento');
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarDepartamento(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo fallo al eliminar el departamento', 400, 'Error al eliminar el departamento');
    })
})

module.exports=router;