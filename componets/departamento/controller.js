const store = require('./store');

function listarDepartamento(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarDepartamento());
    })
}
function agregarDepartamento(departamento){
    return new Promise((resolve, reject)=>{
        if(!departamento.paisDepartamento || !departamento.nombreDepartamento){
            console.error('[Controller Departamento] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullDepartamento = {
            pais : departamento.paisDepartamento,
            nombre : departamento.nombreDepartamento
        }
        resolve(store.agregarDepartamento(fullDepartamento));
    })
}
function actualizarDepartamento(id, departamento){
    return new Promise((resolve, reject)=>{
        if (!id || !departamento.paisDepartamento || !departamento.nombreDepartamento) {
            console.error('[Controller Departamento] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullDepartamento = {
            id, 
            pais : departamento.paisDepartamento,
            nombre : departamento.nombreDepartamento
        }
        resolve(store.actualizarDepartamento(fullDepartamento))
    })
}
function eliminarDepartamento(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Departamento] la informacion no llego');
            reject('Error al inrtentar eiminar');
            return false;
        }
        resolve (store.eliminarDepartamento(id));
    })
}

module.exports = {
    listarDepartamento,
    agregarDepartamento,
    actualizarDepartamento,
    eliminarDepartamento
}