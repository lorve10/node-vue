const {conn} = require('../../settings/db')

function listarDepartamento(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { 
                throw error; 
            }
            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('SELECT id_departamento AS Uid, pais_departamento AS pais, nombre_departamento AS nombre FROM departamentos',(error, result, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarDepartamento(departamento){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('INSERT INTO departamentos (pais_departamento, nombre_departamento) VALUES (?, ?)', [departamento.pais, departamento.nombre], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                    throw error;
                                })
                            }
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarDepartamento(departamento){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error
                }
                connection.query('UPDATE departamentos SET pais_departamento = ?, nombre_departamento = ? WHERE id_departamento = ?', [departamento.pais, departamento.nombre, departamento.id,], (error, results, fields)=>{
                    if (error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }
                        else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[]');
                            reject('');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarDepartamento(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('DELETE FROM departamentos WHERE id_departamento = ?', [id], (error, results, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito')
                        }
                        else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[store Departamento] Algo ocurrio al eliminar el departamento');
                            reject('Fallo la eliminacion del departamento');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarDepartamento,
    agregarDepartamento,
    actualizarDepartamento,
    eliminarDepartamento
}