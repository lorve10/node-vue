const store = require('./store');

function listarCargo(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarCargo());
    })
}

function agregarCargo(cargo){
    return new Promise((resolve, reject)=>{
        if(!cargo.nombre){
            console.error('[Controller Cargo] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        resolve(store.agregarCargo(cargo));
    }) 
}

function actualizarCargo(id, cargo){
    return new Promise((resolve, reject)=>{
        if(!id || !cargo.nombre){
            console.error('[Controller Cargo] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullCargo = {
            id, 
            nombre:cargo.nombre
        }
        resolve(store.actualizarCargo(fullCargo));
    })
}

function eliminarCargo(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Cargo] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarCargo(id));
    })
}

module.exports ={
    listarCargo,
    agregarCargo,
    actualizarCargo,
    eliminarCargo
}