const {conn} = require('../../settings/db'); 

function listarCargo(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('SELECT id_cargo AS Uid, nombre_cargo AS nombre FROM cargos', (error, results, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        resolve(results);
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                    })
                })
            })
        })
    })
}

function agregarCargo(cargo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('INSERT INTO cargos (nombre_cargo) VALUES (?)',[cargo.nombre], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                           connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve('exito');
                    })
                })
            })
        })
    })
}

function actualizarCargo(cargo){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('UPDATE cargos SET nombre_cargo = ? WHERE id_cargo = ? ', [cargo.nombre, cargo.id], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }else if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[Store Cargo] Algo salio mal al elimar el cargo');
                            reject('Error al elimar el cargo algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarCargo(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('DELETE FROM cargos WHERE id_cargo = ?', [id], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }else if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[store Cargo] Algo ocurrio al eliminar el cargo');
                            reject('Fallo la eliminacion del cargo');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarCargo,
    agregarCargo,
    actualizarCargo,
    eliminarCargo
}