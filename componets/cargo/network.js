const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, sAdmin, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarCargo().then((allCargo)=>{
        response.success(req, res, allCargo, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar alos cargos', 500, 'La consulta a los cargos salio mal')
    })
})

router.post('/', [verifyToken, sAdmin, agregar], (req, res)=>{
    controller.agregarCargo(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el cargo', 400, 'Porfavor verificar agregar cargo');
    })
})

router.post('/:Uid', [verifyToken, sAdmin, editar], (req, res)=>{
    controller.actualizarCargo(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el cargo', 500)
    })
})

router.delete('/:Uid', [verifyToken, sAdmin, eliminar], (req, res)=>{
    controller.eliminarCargo(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el cargo',500,'algo ocurrio al eliminar el cargo')
    })
})

module.exports = router;