const store = require('./store');

function listarEstado(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarEstado());
    })
}

function agregarEstado(estado){
    return new Promise((resolve, reject)=>{
        if(!estado.alias || !estado.nombre){
            console.error('[Controller Estado] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullEstado = {
            alias : estado.alias, 
            nombre : estado.nombre
        } 
        resolve(store.agregarEstado(fullEstado));
    }) 
}

function actualizarEstado(id, estado){
    return new Promise((resolve, reject)=>{
        if(!id || !estado.alias || !estado.nombre){
            console.error('[Controller Estado] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullEstado = {
            id,
            alias : estado.alias, 
            nombre : estado.nombre
        }
        resolve(store.actualizarEstado(fullEstado));
    })
}

function eliminarEstado(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Estado] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarEstado(id));
    })
}

module.exports ={
    listarEstado,
    agregarEstado,
    actualizarEstado,
    eliminarEstado
}