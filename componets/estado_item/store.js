const {conn} = require('../../settings/db'); 

function listarEstado(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                
                connection.query('SELECT id_estadoItem AS Uid, alias_estadoItem AS alias, nombre_estadoItem AS nombre FROM estados_items', (error, results, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarEstado(estado){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('INSERT INTO estados_items (alias_estadoItem, nombre_estadoItem) VALUES (?, ?)',[estado.alias, estado.nombre], (error, results, fields)=>{
                    if(error){
                       return connection.rollback(()=>{
                           connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarEstado(estado){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('UPDATE estados_items SET alias_estadoItem = ?, nombre_estadoItem = ? WHERE id_estadoItem = ? ', [estado.alias, estado.nombre, estado.id], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }else if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[Store Estado] Algo salio mal al elimar el estado');
                            reject('Error al elimar el estado algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarEstado(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('DELETE FROM estados_items WHERE id_estadoItem = ?', [id], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }else if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[store Estado] Algo ocurrio al eliminar el estado');
                            reject('Fallo la eliminacion del estado');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarEstado,
    agregarEstado,
    actualizarEstado,
    eliminarEstado
}