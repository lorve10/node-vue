const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarPais().then((allpais)=>{
        response.success(req, res, allpais, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar alos paises', 500, 'La consulta a los paises salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarPais(req.body.nombrePais).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el pais', 400, 'Porfavor verificar agregar pais');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarPais(req.params.Uid, req.body.nombrePais).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el pais', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarPais(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el pais',500,'algo ocurrio al eliminar el pais')
    })
})

module.exports = router;