const store = require('./store');

function listarPais(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarPais());
    })
}

function agregarPais(nombre){
    return new Promise((resolve, reject)=>{
        if(!nombre){
            console.error('[Controller pais] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        resolve(store.agregarPais(nombre));
    }) 
}

function actualizarPais(id, nombre){
    return new Promise((resolve, reject)=>{
        if(!id || !nombre){
            console.error('[Controller pais] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullPais = {
            id, nombre
        }
        resolve(store.actualizarPais(fullPais));
    })
}

function eliminarPais(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller pais] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarPais(id));
    })
}


module.exports ={
    listarPais,
    agregarPais,
    actualizarPais,
    eliminarPais
}