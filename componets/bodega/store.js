const {conn} = require('../../settings/db'); 

function listarBodega(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                
                connection.query('SELECT id_bodega AS Uid, sede_bodega AS sede, usuario_bodega AS usuario, nombre_bodega AS nombre FROM bodegas', (error, results, fields)=>{
                   
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarBodega(bodega){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('INSERT INTO bodegas (sede_bodega, usuario_bodega, nombre_bodega) VALUES (?, ?, ?)',[bodega.sede, bodega.usuario, bodega.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        resolve('exito');
                    })
                })
            })
        })
    })
}

function actualizarBodega(bodega){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('UPDATE bodegas SET sede_bodega = ?, usuario_bodega = ?, nombre_bodega = ? WHERE id_bodega = ? ', [bodega.sede, bodega.usuario, bodega.nombre, bodega.id], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }else if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[Store bodega] Algo salio mal al elimar el bodega');
                            reject('Error al elimar el bodega algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarBodega(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('DELETE FROM bodegas WHERE id_bodega = ?', [id], (error, results, fields)=>{
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }else if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[store bodega] Algo ocurrio al eliminar el bodega');
                            reject('Fallo la eliminacion del bodega');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarBodega,
    agregarBodega,
    actualizarBodega,
    eliminarBodega
}