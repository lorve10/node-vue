const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, sAdmin, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarBodega().then((allBodegas)=>{
        response.success(req, res, allBodegas, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar las bodegas', 500, 'La consulta a las bodegas salio mal')
    })
})

router.post('/', [verifyToken, sAdmin, agregar], (req, res)=>{
    controller.agregarBodega(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el bodega', 400, 'Porfavor verificar agregar bodega');
    })
})

router.post('/:Uid', [verifyToken, sAdmin, editar], (req, res)=>{
    controller.actualizarBodega(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el bodega', 500)
    })
})

router.delete('/:Uid', [verifyToken, sAdmin, eliminar], (req, res)=>{
    controller.eliminarBodega(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el bodega',500,'algo ocurrio al eliminar el bodega')
    })
})

module.exports = router;