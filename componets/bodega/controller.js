const store = require('./store');

function listarBodega(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarBodega());
    })
}

function agregarBodega(bodega){
    return new Promise((resolve, reject)=>{
        if(!bodega.sedeBodega || !bodega.usuarioBodega || !bodega.nombreBodega){
            console.error('[Controller Bodega] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullBodega={ 
            sede : bodega.sedeBodega,
            usuario : bodega.usuarioBodega,
            nombre : bodega.nombreBodega
        }
        resolve(store.agregarBodega(fullBodega));
    }) 
}

function actualizarBodega(id, bodega){
    return new Promise((resolve, reject)=>{
        if(!id || !bodega.sedeBodega || !bodega.usuarioBodega || !bodega.nombreBodega){
            console.error('[Controller Bodega] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullBodega = { 
            id,
            sede : bodega.sedeBodega,
            usuario: bodega.usuarioBodega,
            nombre : bodega.nombreBodega
        }
        resolve(store.actualizarBodega(fullBodega));
    })
}

function eliminarBodega(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Bodega] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarBodega(id));
    })
}


module.exports ={
    listarBodega,
    agregarBodega,
    actualizarBodega,
    eliminarBodega
}