// Network de usuario.
const express = require('express'); //Invocacion de Express.
const jwt = require("jsonwebtoken"); // Invocación del sistema de Tokens.
const response = require('../../network/response'); //Invocaíon del response.
const controller = require('./controller'); // Invocación del controlador.
const {keys} = require('../../settings/key'); // Invocación de la llave secreta.
const router = express.Router(); //Uso de rutas de express.

// Ruta para login.
router.post("/", (req , res) => {
    //Llamado del componente usuario del controlador.
    controller.usuario(req.body)
    .then((fullUser)=>{
        // console.log(fullUser);
        // Validación si llega mensaje de error.
        if (fullUser == "error") {
            response.error(req, res, 'Error informacion invalida', 400, 'este mensaje es para desarrollador'); // Mensaje de validación de error.
            return
        }
        // Inicio de generación de Token si todo sale bien.
        const payload = {
            user: fullUser,
            check:true
        };
        const token = jwt.sign(payload, keys, {
            expiresIn: '8h' // Configuración tiempo de Token (1día).
        });

        const user = {
            apellido: payload.user[0].apellido,
            cargo: payload.user[0].cargo,
            contrasena: payload.user[0].contrasena,
            contrasenaC: payload.user[0].contrasenaC,
            departamento: payload.user[0].departamento,
            direccion: payload.user[0].direccion,
            email: payload.user[0].email,
            foto: payload.user[0].foto,
            identificacion: payload.user[0].identificacion,
            municipio: payload.user[0].municipio,
            nacimiento: payload.user[0].nacimiento,
            nombre: payload.user[0].nombre,
            permiso: payload.user[0].permiso,
            telefono: payload.user[0].telefono,
            usuario: payload.user[0].usuario
        }

        // Fin de Generacion de Token.
        response.success(req, res, {token, usuario:JSON.stringify(user)}, 201); // Mensaje de validación de exito.
    })
    .catch(e=>{
        console.log(e);
        response.error(req, res, 'Error informacion invalida', 400, 'este mensaje es para desarrollador'); // Mensaje de validación de error.
    });
});

//Exportamos las funciones (modulos).
module.exports = router;