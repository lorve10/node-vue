// Store usuario.
const {
    conn
} = require('../../settings/db'); // Llama al archivo de conexion.
const bcrypt = require('bcrypt'); // Llama al sistema de encriptacion de contraseña.
// const saltRounds = 8;// Configracion de la encriptacion de la cotraseña.

// Funcion para la comprobacion del usuario
function login(usuario) {
    // Devolucion de una promesa.
    return new Promise((resolve, reject) => {
        // Abrimos una conexion a la base de datos.
        conn.getConnection((error, connection) => {
            if (error) {
                connection.release(); // Devolvemos la conexion/cerramos conexión.
                console.log(error);
                throw error;
            } // Mensaje de error.
            // Inicio de transacción.
            connection.beginTransaction(function (err) {
                if (err) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    console.log(err);
                    throw err;
                } // Mensaje de error.
                // connection.query('SELECT usuarios.id_usuario AS Uid, usuarios.nombre_usuario AS usuario FROM usuarios WHERE (rol_usuario = 0) AND (superAdmin_usuario = 1) AND (nombre_usuario = ?)',[usuario.user], (erre, result, fields) => {
                connection.query('SELECT usuarios.id_usuario AS Uid, usuarios.nombre_usuario AS usuario FROM usuarios WHERE (estado_usuario = 1) AND (nombre_usuario = ?)', [usuario.user], (erre, result, fields) => {
                    if (erre) {
                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                        return connection.rollback(() => {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.log(erre);
                            throw erre; // Mensaje de error.
                        });
                    }
                    ///PRUEBA
                    console.log(result);
                    ////

                    if (result.length > 0) {
                        var uid = result[0].Uid;
                        connection.query('SELECT contrasena_usuario AS password, rol_usuario AS rol, id_permiso AS permiso, foto_usuario AS foto, nombre_usuario AS usuario, identificacion_persona AS identificacion, nombre_persona AS nombre, apellido_persona AS apellido, nombre_cargo AS cargo, domicilio_persona AS direccion, DATE_FORMAT(fechaNacimiento_persona, "%Y-%m-%d") AS nacimiento, departamento_municipio AS Uid_depatamento,  municipioNacimiento_persona AS Uid_municipio, telefono_persona AS telefono, correo_persona AS email FROM usuarios INNER JOIN cargos ON cargo_usuario = id_cargo INNER JOIN personas ON persona_usuario = id_persona LEFT JOIN municipios ON id_municipio = municipioNacimiento_persona LEFT JOIN departamentos ON id_departamento = departamento_municipio INNER JOIN roles ON rol_usuario = id_rol LEFT JOIN roles_permisos ON rol_rolPermiso = id_rol LEFT JOIN permisos ON permiso_rolPermiso = id_permiso WHERE (usuarios.id_usuario = ?)', [uid], (errer, results, fields) => {

                            if (errer) {
                                //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                return connection.rollback(() => {
                                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                                    console.log(errer);
                                    throw errer; // Mensaje de error.
                                });
                            }

                            connection.query('SELECT rol_rolPermiso AS rol, permiso_rolPermiso AS permiso FROM usuarios INNER JOIN roles ON rol_usuario = id_rol INNER JOIN roles_permisos ON rol_rolPermiso = id_rol INNER JOIN permisos ON permiso_rolPermiso = id_permiso WHERE (id_usuario = ?)', [uid], (errer, resultRol, fields) => {
                                if (errer) {
                                    //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                    return connection.rollback(() => {
                                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                                        console.log(errer);
                                        throw errer; // Mensaje de error.
                                    });
                                }

                                const permiso = [];
                                if (resultRol[0].rol == 0) {
                                    permiso.push(resultRol[0].rol);
                                }else{
                                    resultRol.forEach(element => {
                                        permiso.push(element.permiso);
                                    });
                                }

                                // Guada guada los cambios en la base de datos si todo sale bien en la transaccion
                                connection.commit((errerr) => {
                                    if (errerr) {
                                        //Devolucion de todos los cambios de la base de datos sia salgo sale mal en la transacción.
                                        return connection.rollback(() => {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            console.log(errerr);
                                            throw errerr; // Mensaje de error.
                                        });
                                    }
                                    var pass = results[0].password;
                                    // Validación si la contraseña es correcta.
                                    bcrypt.compare(usuario.password, pass, (erroor, res) => {
                                        if (erroor) {
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            console.log(erroor);
                                            throw erroor; // Mensaje de error.
                                        }
                                        if (res) {
                                            const fullUsuario = []
                                            results.forEach(element => {
                                                fullUsuario.push({
                                                    rol: resultRol[0].rol,
                                                    permiso: permiso,
                                                    foto: element.foto,
                                                    contrasena: '',
                                                    contrasenaC: '',
                                                    usuario: element.usuario,
                                                    identificacion: element.identificacion,
                                                    nombre: element.nombre,
                                                    apellido: element.apellido,
                                                    cargo: element.cargo,
                                                    direccion: element.direccion,
                                                    nacimiento: element.nacimiento,
                                                    departamento: element.Uid_depatamento,
                                                    municipio: element.Uid_municipio,
                                                    telefono: element.telefono,
                                                    email: element.email
                                                })
                                            });
                                            resolve(fullUsuario); //Resolver y devolver la promesa exitosa.
                                            console.log(fullUsuario);
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                        }
                                        if (!res) {
                                            // Devolución de errores.
                                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                                            console.error('[MessageController] Usuario o contraseña errados');
                                            reject('Usuario o contraseña errados');
                                            return false;
                                        }
                                    });
                                });
                            });
                        });
                    } else {
                        // Devolución de errores.
                        connection.release();
                        console.error('[MessageController] Usuario o contraseña errados');
                        reject('Usuario o contraseña errados');
                        return false;
                    }
                });
            });
        });
    })
}

// Exportamos las funciones (modulos).
module.exports = {
    login
}
