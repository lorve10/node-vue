// Controlador usuario.
const store = require('./store'); // Invocación del archivo store.

// Funcion para comprobar usuario.
 function usuario(datos){
    // Devolucion de una promesa.
    return new Promise((resolve, reject)=>{
        // Comprovación si no llega campos vacios.
        if (!datos.usuario || !datos.contrasena) {
            // Devolución de errores.
            console.error('[MessageController] No hay usuario o contraseña');
            reject('Los datos son incorrectos');
            return false;
        }else{
            // Llena array con datos enviados por el usuario.
            const fullUser = {
                user:datos.usuario,
                password: datos.contrasena
            };
            
            // Comprovación de error al encontar usuario.
            resolve(store.login(fullUser)); // Resolver y devolver la promesa exitosa.
        }
    })
}


// Exportamos las funciones (modulos).
module.exports = { usuario }