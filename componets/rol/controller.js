const store = require('./store');

function listarRol(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarRol());
    })
}

function agregarRol(rol){
    return new Promise((resolve, reject)=>{
        if(!rol.nombre){
            console.error('[Controller Rol] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        const fullRol = {
            nombre: rol.nombre,
            permis: rol.permiso
        }
        resolve(store.agregarRol(fullRol));
    }) 
}

function actualizarRol(id, rol){
    return new Promise((resolve, reject)=>{
        if(!id || !rol.nombre){
            console.error('[Controller rol] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullRol = {
            id, 
            nombre: rol.nombre,
            permis: rol.permiso
        }
        resolve(store.actualizarRol(fullRol));
    })
}

function eliminarRol(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller rol] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarRol(id));
    })
}

module.exports ={
    listarRol,
    agregarRol,
    actualizarRol,
    eliminarRol
}