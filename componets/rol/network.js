const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, sAdmin, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarRol().then((allRol)=>{
        response.success(req, res, allRol, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar alos roles', 500, 'La consulta a los roles salio mal')
    })
})

router.post('/', [verifyToken, sAdmin, agregar], (req, res)=>{
    controller.agregarRol(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el rol', 400, 'Porfavor verificar agregar Rol');
    })
})

router.post('/:Uid', [verifyToken, sAdmin, editar], (req, res)=>{
    controller.actualizarRol(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)

    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el rol', 500)
    })
})

router.delete('/:Uid', [verifyToken, sAdmin, eliminar], (req, res)=>{
    controller.eliminarRol(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el rol',500,'algo ocurrio al eliminar el rol')
    })
})

module.exports = router;