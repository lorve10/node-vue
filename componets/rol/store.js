const {
    conn
} = require('../../settings/db');

function listarRol() {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {

            if (error) {
                throw error;
            }

            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }

                connection.query('SELECT id_rol AS Uid, nombre_rol AS nombre FROM roles WHERE id_rol != 0', (error, results, fields) => {
                    if (error) {
                        connection.release();
                        return connection.rollback(() => {
                            throw error;
                        });
                    }
                    var i = 0;
                    const user = []
                    results.forEach(element => {
                        connection.query('SELECT id_permiso AS Uid, nombre_permiso AS nombre FROM roles LEFT JOIN roles_permisos ON id_rol = rol_rolPermiso LEFT JOIN permisos ON id_permiso = permiso_rolPermiso WHERE id_rol = ?', [element.Uid], (error, result, fields) => {

                            if (error) {
                                return connection.rollback(() => {
                                    throw error;
                                });
                            }

                            user.push({
                                    Uid: results[i].Uid,
                                    nombre: results[i].nombre,
                                    permiso: result
                                })
                                ++i
                            if (results.length == i) {
                                connection.release();
                                connection.commit((error) => {
                                    if (error) {
                                        return connection.rollback(() => {
                                            throw error;
                                        })
                                    }
                                    resolve(user);
                                })
                            }

                        })
                    });
                })
            })
        })
    })
}

function agregarRol(rol) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('INSERT INTO roles (nombre_rol) VALUES (?)', [rol.nombre], (error, results, fields) => {
                    var i = 0;
                    if (rol.permis.length > 0) {
                        rol.permis.forEach(element => {
                            connection.query('INSERT INTO roles_permisos (rol_rolPermiso, permiso_rolPermiso) VALUES (?, ?)', [results.insertId, element.Uid], (error, resultt, fields) => {
                                i = i + 1;
                                if (error) {
                                    return connection.rollback(() => {
                                        connection.release();
                                        throw error;
                                    })
                                }
                                if (rol.permis.length == i) {
                                    //                 connection.release();
                                    connection.release();
                                    connection.commit((error) => {
                                        if (error) {
                                            return connection.rollback(() => {
                                                throw error;
                                            })
                                        }
                                        resolve('exito');
                                    })
                                }
                            })
                        });
                    } else {
                        connection.release();
                        connection.commit((error) => {
                            if (error) {
                                return connection.rollback(() => {
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    }
                })
            })
        })
    })
}

function actualizarRol(rol) {

    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }

                connection.query('UPDATE roles SET nombre_rol = ? WHERE id_rol = ? ', [rol.nombre, rol.id], (error, results, fields) => {

                    if (error) {
                        connection.release();
                        return connection.rollback(() => {
                            throw error;
                        })
                    }
                    connection.query('DELETE FROM roles_permisos WHERE rol_rolPermiso = ?', [rol.id], (error, result, fields) => {
                        if (error) {
                            connection.release();
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        var i = 0;
                        if (rol.permis.length > 0) {
                            rol.permis.forEach(element => {
                                connection.query('INSERT INTO roles_permisos (rol_rolPermiso, permiso_rolPermiso) VALUES (?, ?)', [rol.id, element.Uid], (error, resultt, fields) => {
                                    i = i + 1;
                                    if (error) {
                                        return connection.rollback(() => {
                                            throw error;
                                        })
                                    }
                                    if (rol.permis.length == i) {
                                        //                 connection.release();
                                        connection.commit((error) => {
                                            if (error) {
                                                return connection.rollback(() => {
                                                    throw error;
                                                })
                                            } else if (results.affectedRows) {
                                                resolve('exito');
                                            } else {
                                                console.error('[Store rol] Algo salio mal al elimar el rol');
                                                reject('Error al elimar el rol algo salio mal');
                                                return false;
                                            }
                                        })
                                    }
                                })
                            });
                        } else {
                            connection.commit((error) => {
                                if (error) {
                                    return connection.rollback(() => {
                                        throw error;
                                    })
                                } else if (results.affectedRows) {
                                    resolve('exito');
                                } else {
                                    console.error('[Store rol] Algo salio mal al elimar el rol');
                                    reject('Error al elimar el rol algo salio mal');
                                    return false;
                                }
                            })
                        }
                    });
                })
            })
        })
    })
}

function eliminarRol(id) {
    return new Promise((resolve, reject) => {
        conn.getConnection((error, connection) => {
            if (error) {
                throw error;
            }
            connection.beginTransaction((error) => {
                if (error) {
                    throw error;
                }
                connection.query('DELETE FROM roles_permisos WHERE rol_rolPermiso = ?', [id], (error, results, fields) => {
                    if (error) {
                        return connection.rollback(() => {
                            connection.release();
                            throw error;
                        })
                    }
                    connection.query('DELETE FROM roles WHERE id_rol = ?', [id], (error, results, fields) => {
                        connection.release();
                        if (error) {
                            return connection.rollback(() => {
                                throw error;
                            })
                        }
                        connection.commit((error) => {
                            if (error) {
                                return connection.rollback(() => {
                                    throw error;
                                })
                            } else if (results.affectedRows) {
                                resolve('exito');
                            } else {
                                console.error('[store rol] Algo ocurrio al eliminar el rol');
                                reject('Fallo la eliminacion del rol');
                                return false;
                            }
                        });
                    });
                });
            })
        })
    })
}

module.exports = {
    listarRol,
    agregarRol,
    actualizarRol,
    eliminarRol
}