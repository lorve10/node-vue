const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, sAdmin, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.


router.get('/', verifyToken, (req, res)=>{
    controller.listarEmpresas()
    .then((empresas)=>{
        response.success(req, res, empresas, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal', 500, 'error al listar las empresas');
    })
})

router.post('/', [verifyToken, sAdmin, agregar], (req, res) => {
    controller.agregarEmpresa(req.body).then((fullEmpresa) => {
        response.success(req, res, fullEmpresa, 201);
    })
    .catch(e=>{
        response.error(req, res, 'Por favor valide la informacion', 400, 'Mensaje interno');
    })
})

router.post('/:Uid', [verifyToken, sAdmin, editar], (req, res)=>{
    controller.actualizarEmpresa(req.params.Uid, req.body).then((fullEmpresa)=>{
        response.success(req, res, fullEmpresa, 201)
    }).catch(e=>{
        response.error(req, res,'No se pudo actualizar la empresa', 500, 'Verificar consulta de actualizacionde empresa')
    })
})

router.delete('/:Uid', [verifyToken, sAdmin, eliminar], (req, res)=>{
    controller.eliminarEmpresa(req.params.Uid).then((fullEmpresa)=>{
        response.success(req, res, fullEmpresa, 201);
    }).catch(e=>{
        response.error(req, res,'No se pudo Elimar la empresa', 500, 'Verificar consulta de eliminar empresa')
    })
})


module.exports = router;