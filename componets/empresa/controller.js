const store = require('./store');

function listarEmpresas(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarEmpresas());
    })
}

function agregarEmpresa(empresa) {
    return new Promise((resolve, reject)=>{
        if (!empresa.nombreEmpresa) {
            console.error('[MessageController] No se resivio un valor en el nombre');
            reject('Los datos son errados');
            return false;
        }
        const fullEmpresa={
            nombre: empresa.nombreEmpresa
        }
        resolve(store.agregarEmpresa(fullEmpresa));
    })
}

function actualizarEmpresa(Uid, empresa){
    return new Promise((resolve, reject)=>{
        if (!Uid || !empresa.nombreEmpresa) {
            console.error('[Controlador Empresa actualizar] No se recivieron los campos correctamenet');
            reject('Error los datos se envaron mal');
            return false;
        }
        fullEmpresa={
            Uid,
            nombre: empresa.nombreEmpresa
        }
        resolve(store.actualizarEmpresa(fullEmpresa));
    })
}

function eliminarEmpresa(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('El id se encuentra errado');
            reject('Error al eliminar la empresa')
            return false;
        }
        resolve(store.eliminarEmpresa(id));
    })
}

module.exports={
    listarEmpresas,
    agregarEmpresa,
    actualizarEmpresa,
    eliminarEmpresa
}