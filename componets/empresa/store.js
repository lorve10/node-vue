const {conn} = require('../../settings/db');

function listarEmpresas(){
    return new Promise((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
            connection.beginTransaction((error) =>{ 
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión. 
                    throw error; 
                }
                connection.query('SELECT id_empresa AS Uid, nombre_empresa AS nombre FROM empresas', (err, result, files) => {
                    if (err) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw err;
                        })
                    }
                    connection.commit((errorCommit)=>{
                        if (errorCommit) {
                            return connection.rollback(() => {
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw errorCommit;
                            })
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve(result);
                    })
                })
            })
        })
    })
}

function agregarEmpresa(empresa) {
    return new Promise((resolve, reject) => {
        conn.getConnection( (errCone, connection)=>{
            if (errCone) {
                throw errCone;
            }
            connection.beginTransaction((err)=>{
                if (err) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw err;
                }
                connection.query('INSERT INTO empresas (nombre_empresa) VALUES (?)',[empresa.nombre], (error, result, fields) => {
                    if(error){
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        });
                    }
                    connection.commit((errorComm)=>{
                        if(errorComm){
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw errorComm;
                            });
                        }
                        connection.release(); // Devolvemos la conexion/cerramos conexión.
                        resolve('exito');
                    })
                })
           })
        })
    })
}

function actualizarEmpresa(empresa) {
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('UPDATE empresas SET nombre_empresa = ? WHERE id_empresa = ?', [empresa.nombre, empresa.Uid], (error, results, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((erro)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        if(results.affectedRows){
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[MessageController] Empresa no encontrada');
                            reject('Los datos son incorrectos2');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

function eliminarEmpresa(Uid){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {
                throw error;
            }
            connection.beginTransaction((error)=>{
                if (error) {
                    connection.release(); // Devolvemos la conexion/cerramos conexión.
                    throw error;
                }
                connection.query('DELETE FROM empresas WHERE id_empresa = ?', [Uid], 
                (error, results, fields)=>{
                    if (error) {
                        return connection.rollback(()=>{
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return rollback(()=>{
                                connection.release(); // Devolvemos la conexion/cerramos conexión.
                                throw error;
                            })
                        }
                        if (results.affectedRows) {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            resolve('exito');
                        }else {
                            connection.release(); // Devolvemos la conexion/cerramos conexión.
                            console.error('[Store empresa] error al eliminar verificar sql');
                            reject('Errror no se elimino la empresa');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

module.exports = {
    listarEmpresas,
    agregarEmpresa,
    actualizarEmpresa,
    eliminarEmpresa
}