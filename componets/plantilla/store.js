const {conn} = require('../../settings/db'); 

function listarPlantilla(){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            
            if (error) {throw error;}

            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                
                connection.query('SELECT id_plantilla AS Uid, nombre_plantilla AS nombre FROM plantillas', (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        });
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(results);
                    })
                })
            })
        })
    })
}

function agregarPlantilla(nombre){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('INSERT INTO plantillas (nombre_plantilla) VALUES (?)',[nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                       return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve('exito');
                        
                    })
                })
            })
        })
    })
}

function actualizarPlantilla(plantilla){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){
                    throw error;
                }
                connection.query('UPDATE plantillas SET nombre_plantilla = ? WHERE id_plantilla = ? ', [plantilla.nombre, plantilla.id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[Store Plantilla] Algo salio mal al actualizar la plnatilla');
                            reject('Error al actualizar la Plantilla algo salio mal');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarPlantilla(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM plantillas WHERE id_plantilla = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }else if(results.affectedRows){
                            resolve('exito');
                        }else{
                            console.error('[store Plantilla] Algo ocurrio al eliminar la plantilla');
                            reject('Fallo la eliminacion de la Plantilla');
                            return false;
                        }
                    });
                });
            })
        })
    })
}

module.exports={
    listarPlantilla,
    agregarPlantilla,
    actualizarPlantilla,
    eliminarPlantilla
}