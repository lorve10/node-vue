const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarPlantilla().then((allPlantilla)=>{
        response.success(req, res, allPlantilla, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar las plantillas', 500, 'La consulta plantillas salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarPlantilla(req.body.nombrePlantilla).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar la plantilla', 400, 'Porfavor verificar agregar la plantilla');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarPlantilla(req.params.Uid, req.body.nombrePlantilla).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar la plantilla', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarPlantilla(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar laplantilla',500,'algo ocurrio al eliminar la plantilla')
    })
})

module.exports = router;