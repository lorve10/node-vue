const store = require('./store');

function listarPlantilla(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarPlantilla());
    })
}

function agregarPlantilla(nombre){
    return new Promise((resolve, reject)=>{
        if(!nombre){
            console.error('[Controller Plantilla] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        resolve(store.agregarPlantilla(nombre));
    }) 
}

function actualizarPlantilla(id, nombre){
    return new Promise((resolve, reject)=>{
        if(!id || !nombre){
            console.error('[Controller Plantilla] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullPlantilla = {
            id, nombre
        }
        resolve(store.actualizarPlantilla(fullPlantilla));
    })
}

function eliminarPlantilla(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Plantilla] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarPlantilla(id));
    })
}

module.exports ={
    listarPlantilla,
    agregarPlantilla,
    actualizarPlantilla,
    eliminarPlantilla
}