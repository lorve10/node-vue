const {conn} = require('../../settings/db')

function listarProveedor(){
    return new Promise ((resolve, reject)=>{

        conn.getConnection((error, connection)=>{

            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_proveedor AS Uid, municipio_proveedor AS municipio, nombre_proveedor AS nombre, telefono_proveedor AS telefono, correo_proveedor AS email, direccion_proveedor AS direccion, web_proveedor AS web FROM proveedores', (error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarProveedor(proveedor){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO proveedores (nombre_proveedor, telefono_proveedor, correo_proveedor, direccion_proveedor) VALUES (?, ?, ?, ?)', [ proveedor.nombre, proveedor.telefono, proveedor.email, proveedor.direccion], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarProveedor(proveedor){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error) {throw error}
                connection.query('UPDATE proveedores SET  nombre_proveedor = ?, telefono_proveedor = ?, correo_proveedor = ?, direccion_proveedor = ? WHERE id_proveedor = ?', [ proveedor.nombre, proveedor.telefono, proveedor.email, proveedor.direccion, proveedor.id], (error, results, fields)=>{
                    connection.release();
                    if (error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito');
                        }
                        else{
                            console.error('[]');
                            reject('');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarProveedor(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM proveedores WHERE id_proveedor = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito')
                        }
                        else{
                            console.error('[store Proveedor] Algo ocurrio al eliminar el proveedor');
                            reject('Fallo la eliminacion de el proveedor');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarProveedor,
    agregarProveedor,
    actualizarProveedor,
    eliminarProveedor
}
