const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarProveedor().then((allProveedor)=>{
        response.success(req, res, allProveedor, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar a los proveedores', 500, 'La consulta a los proveedores salio mal');
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarProveedor(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al agregar el proveedor', 400, 'Error al agregar el proveedor');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarProveedor(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al intentar actualizar el proveedor', 400, 'Error al actualizar el proveedor');
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarProveedor(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo fallo al eliminar el proveedor', 400, 'Error al eliminar el proveedor');
    })
})

module.exports=router;