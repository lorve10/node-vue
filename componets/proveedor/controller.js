const store = require('./store');

function listarProveedor(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarProveedor());
    })
}
function agregarProveedor(proveedor){
    return new Promise((resolve, reject)=>{
      console.log(proveedor);
        if(!proveedor.nombre || !proveedor.telefono|| !proveedor.email || !proveedor.direccion){
            console.error('[Controller Proveedor] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullProveedor = {
            nombre : proveedor.nombre,
            telefono : proveedor.telefono,
            email : proveedor.email,
            direccion : proveedor.direccion,
        }
        resolve(store.agregarProveedor(fullProveedor));
    })
}
function actualizarProveedor(id, proveedor){
    return new Promise((resolve, reject)=>{
        if (!id ||  !proveedor.nombre || !proveedor.telefono || !proveedor.email || !proveedor.direccion ) {
            console.error('[Controller Proveedor] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullProveedor = {
            id,
            nombre : proveedor.nombre,
            telefono : proveedor.telefono,
            email : proveedor.email,
            direccion : proveedor.direccion,
        }
        resolve(store.actualizarProveedor(fullProveedor))
    })
}
function eliminarProveedor(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Proveedor] la informacion no llego');
            reject('Error al inrtentar eiminar');
            return false;
        }
        resolve (store.eliminarProveedor(id));
    })
}

module.exports = {
    listarProveedor,
    agregarProveedor,
    actualizarProveedor,
    eliminarProveedor
}
