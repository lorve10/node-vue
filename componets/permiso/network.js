const express = require('express'); // Invocacion de
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarPermiso().then((allPermiso)=>{
        response.success(req, res, allPermiso, 200);
    }).catch(e=>{
        response.error(req, res, 'No se pudo listar alos permisos', 500, 'La consulta a los permisos salio mal')
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarPermiso(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al agregar el permiso', 400, 'Porfavor verificar agregar permiso');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarPermiso(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200)
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al actualizar el permiso', 500)
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarPermiso(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Error al eliminar el permiso',500,'algo ocurrio al eliminar el permiso')
    })
})

module.exports = router;