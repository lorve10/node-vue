const store = require('./store');

function listarPermiso(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarPermiso());
    })
}

function agregarPermiso(permiso){
    return new Promise((resolve, reject)=>{
        if(!permiso.nombre){
            console.error('[Controller permiso] No se eviaron los campos requeridos');
            reject('Valida la informacion');
            return false;
        }
        resolve(store.agregarPermiso(permiso));
    }) 
}

function actualizarPermiso(id, permiso){
    return new Promise((resolve, reject)=>{
        if(!id || !permiso.nombre){
            console.error('[Controller permiso] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullPermiso = {
            id, 
            nombre: permiso.nombre
        }
        resolve(store.actualizarPermiso(fullPermiso));
    })
}

function eliminarPermiso(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller permiso] la informacion no llego');
            reject('Valida que los campos');
            return false;
        }
        resolve(store.eliminarPermiso(id));
    })
}

module.exports ={
    listarPermiso,
    agregarPermiso,
    actualizarPermiso,
    eliminarPermiso
}