const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, sAdmin, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarSede().then((allSede)=>{
        response.success(req, res, allSede, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar a las sede', 500, 'La consulta a las sede salio mal');
    })
})

router.post('/', [verifyToken, sAdmin, agregar], (req, res)=>{
    controller.agregarSede(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al agregar la sede', 400, 'Error al agregar la sede');
    })
})

router.post('/:Uid', [verifyToken, sAdmin, editar], (req, res)=>{
    controller.actualizarSede(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo salio mal al intentar actualizar la sede', 400, 'Error al actualizar la sede');
    })
})

router.delete('/:Uid', [verifyToken, sAdmin, eliminar], (req, res)=>{
    controller.eliminarSede(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, 'Algo fallo al eliminar la sede', 400, 'Error al eliminar la sede');
    })
})

module.exports=router;