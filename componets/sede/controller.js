const store = require('./store');

function listarSede(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarSede());
    })
}
function agregarSede(sede){
    return new Promise((resolve, reject)=>{
        if(!sede.municipioSede || !sede.empresaSede || !sede.nombreSede || !sede.direccionSede){
            console.error('[Controller Sede] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullSede = {
            municipio : sede.municipioSede,
            empresa : sede.empresaSede,
            nombre : sede.nombreSede,
            direccion : sede.direccionSede
        }
        resolve(store.agregarSede(fullSede));
    })
}
function actualizarSede(id, sede){
    return new Promise((resolve, reject)=>{
        if (!id || !sede.municipioSede || !sede.empresaSede || !sede.nombreSede || !sede.direccionSede) {
            console.error('[Controller Sede] Verificar los campos alguno esta vacio');
            reject('Algun campo es vacio');
            return false;
        }
        const fullSede = {
            id, 
            municipio : sede.municipioSede,
            empresa : sede.empresaSede,
            nombre : sede.nombreSede,
            direccion : sede.direccionSede
        }
        resolve(store.actualizarSede(fullSede))
    })
}
function eliminarSede(id){
    return new Promise((resolve, reject)=>{
        if(!id){
            console.error('[Controller Sede] la informacion no llego');
            reject('Error al inrtentar eiminar');
            return false;
        }
        resolve (store.eliminarSede(id));
    })
}

module.exports = {
    listarSede,
    agregarSede,
    actualizarSede,
    eliminarSede
}