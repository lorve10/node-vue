const {conn} = require('../../settings/db')

function listarSede(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_sede AS Uid, departamento_municipio AS UidDepartamento, municipio_sede AS UidMunicipio, nombre_municipio AS municipio, empresa_sede AS Uid_empresa, nombre_empresa AS empresa, nombre_sede AS nombre, direccion_sede AS direccion FROM municipios INNER JOIN sedes ON id_municipio=municipio_sede INNER JOIN departamentos ON id_departamento = departamento_municipio INNER JOIN empresas ON empresa_sede=id_empresa',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarSede(sede){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO sedes (municipio_sede, empresa_sede, nombre_sede, direccion_sede) VALUES (?, ?, ?, ?)', [sede.municipio, sede.empresa, sede.nombre, sede.direccion], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarSede(sede){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error) {throw error}
                connection.query('UPDATE sedes SET municipio_sede = ?, empresa_sede = ?, nombre_sede = ?, direccion_sede = ? WHERE id_sede = ?', [sede.municipio, sede.empresa, sede.nombre, sede.direccion, sede.id], (error, results, fields)=>{
                    connection.release();
                    if (error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito');
                        }
                        else{
                            console.error('[]');
                            reject('');
                            return false;
                        }
                    })
                })
            })
        })
    })
}

function eliminarSede(id){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){throw error;}
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('DELETE FROM sedes WHERE id_sede = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        else if (results.affectedRows) {
                            resolve('exito')
                        }
                        else{
                            console.error('[store Sede] Algo ocurrio al eliminar la sede');
                            reject('Fallo la eliminacion de la sede');
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarSede,
    agregarSede,
    actualizarSede,
    eliminarSede
}