const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');
const {verifyToken, agregar, editar, eliminar} = require('../../network/valiToken'); //Validacion del token.

router.get('/', verifyToken, (req, res)=>{
    controller.listarRemisImg().then((allRemisImg)=>{
        response.success(req, res, allRemisImg, 200);
    }).catch(e=>{
        response.error(req, res,'No se pudo listar las imagenes', 500, 'La consulta las imagenes salio mal');
    })
})

router.post('/', [verifyToken, agregar], (req, res)=>{
    controller.agregarRemisImg(req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, '', 400, '');
    })
})

router.post('/:Uid', [verifyToken, editar], (req, res)=>{
    controller.actualizarRemisImg(req.params.Uid, req.body).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, '', 400, '');
    })
})

router.delete('/:Uid', [verifyToken, eliminar], (req, res)=>{
    controller.eliminarRemisImg(req.params.Uid).then((result)=>{
        response.success(req, res, result, 200);
    }).catch(e=>{
        response.error(req, res, '', 400, '');
    })
})

module.exports=router;