const {conn} = require('../../settings/db')

function listarRemisImg(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_remisionImagen AS Uid, remision_remisionImagen AS remision, imagen_remisionImagen AS imagen FROM remisiones_imagenes',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarRemisImg(imagen){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO remisiones_imagenes (remision_remisionImagen, imagen_remisionImagen) VALUES (?, ?)', [imagen.remision, imagen.imagen], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarRemisImg(imagen){
    return new Promise ((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error}
                connection.query('UPDATE remisiones_imagenes SET remision_remisionImagen = ?, imagen_remisionImagen = ? WHERE id_remisionImagen = ?', [imagen.remision, imagen.imagen, imagen.id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{throw error;})
                    }

                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{throw error;})
                        }
                        else if(results.affectedRows){
                            resolve('exito');
                        }
                        else {
                            console.error('No se pudo actualizar la remision');
                            reject('Algho salio mal al actualizar la remision');
                            return false;
                        }
                    });

                })
            })
        })
    })
}

function eliminarRemisImg(id){
    return new Promise((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){ throw error; }
                connection.query('DELETE FROM remisiones_imagenes WHERE id_remisionImagen = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){ 
                        connection.rollback(()=>{ 
                        throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){connection.rollback(()=>{
                                throw error
                            })
                        }
                        else if(results.affectedRows){
                            resolve('exito')
                        }else{
                            console.error('No se pudo eliminar la imagen');
                            reject('Intenta nuevamente eliminar la imagen')
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarRemisImg,
    agregarRemisImg,
    actualizarRemisImg,
    eliminarRemisImg
}