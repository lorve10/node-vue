const store = require('./store');

function listarRemisImg(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarRemisImg());
    })
}
function agregarRemisImg(imagen){
    return new Promise((resolve, reject)=>{
        if(!imagen.remisionImagen || !imagen.imagen){
            console.error('[Controller remision_imagen] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullRemisImg = {
            remision : imagen.remisionImagen,
            imagen : imagen.imagen
        }
        resolve(store.agregarRemisImg(fullRemisImg));
    })
}
function actualizarRemisImg(id, imagen){
    return new Promise((resolve, reject)=>{
        if(!id || !imagen.remisionImagen || !imagen.imagen){
            console.error('[Controller remision_imagen]');
            reject('');
            return false;
        }
        const fullRemisImg = { 
            id, 
            remision : imagen.remisionImagen,
            imagen : imagen.imagen
        }
        resolve(store.actualizarRemisImg(fullRemisImg))
    })
}
function eliminarRemisImg(id){
    return new Promise((resolve, reject)=>{
        if (!id) {
            console.error('[Controller remision_imagen] el identificador de la imagen es vacio');
            reject('Verificar campo llego vacio');
            return false;
        }
        resolve(store.eliminarRemisImg(id));
    })
}

module.exports = {
    listarRemisImg,
    agregarRemisImg,
    actualizarRemisImg,
    eliminarRemisImg
}