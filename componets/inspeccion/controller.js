const store = require('./store');

function listarInspeccion(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarInspeccion());
    })
}
function agregarInspeccion(formato){
    return new Promise((resolve, reject)=>{
        if(!formato.plantillaFormato || !formato.nombreformato){
            console.error('[Controller formato] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullFormato = {
            plantilla : formato.plantillaFormato,
            nombre : formato.nombreformato
        }
        resolve(store.agregarInspeccion(fullFormato));
    })
}
function actualizarInspeccion(id, formato){
    return new Promise((resolve, reject)=>{
        if(!id || !formato.plantillaFormato || !formato.nombreFormato){
            console.error('[Controller formato]');
            reject('');
            return false;
        }
        const fullFormato = { 
            id, 
            plantilla : formato.plantillaFormato,
            nombre : formato.nombreFormato
        }
        resolve(store.actualizarInspeccion(fullFormato))
    })
}
function eliminarInspeccion(id){
    return new Promise((resolve, reject)=>{
        if (!id) {
            console.error('[Controller formato] el identificador del formato es vacio');
            reject('Verificar campo llego vacio');
            return false;
        }
        resolve(store.eliminarInspeccion(id));
    })
}

module.exports = {
    listarInspeccion,
    agregarInspeccion,
    actualizarInspeccion,
    eliminarInspeccion
}