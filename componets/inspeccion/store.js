const {conn} = require('../../settings/db')

function listarInspeccion(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_inspeccion AS Uid, item_inspeccion AS item, nombre_inspeccion AS nombre, archivo_inspeccion AS archivo, archivo_inspeccionImagen AS imagen FROM inspecciones INNER JOIN inspecciones_imagenes ON id_inspeccion = id_inspeccionImagen;',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarInspeccion(formato){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO formatos (plantilla_formato, nombre_formato) VALUES (?, ?)', [formato.plantilla, formato.nombre], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarInspeccion(formato){
    return new Promise ((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error}
                connection.query('UPDATE formatos SET plantilla_formato = ?, nombre_formato = ? WHERE id_formato = ?', [formato.plantilla, formato.nombre, formato.id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{throw error;})
                    }

                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{throw error;})
                        }
                        else if(results.affectedRows){
                            resolve('exito');
                        }
                        else {
                            console.error('No se pudo actualizar el formato');
                            reject('Algho salio mal al actualizar el formato');
                            return false;
                        }
                    });

                })
            })
        })
    })
}

function eliminarInspeccion(id){
    return new Promise((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){ throw error; }
                connection.query('DELETE FROM formatos WHERE id_formato = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){ 
                        connection.rollback(()=>{ 
                        throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){connection.rollback(()=>{
                                throw error
                            })
                        }
                        else if(results.affectedRows){
                            resolve('exito')
                        }else{
                            console.error('No se pudo eliminar el formato');
                            reject('Intenta nuevamente eliminar el formato')
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarInspeccion,
    agregarInspeccion,
    actualizarInspeccion,
    eliminarInspeccion
}