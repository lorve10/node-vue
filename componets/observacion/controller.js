const store = require('./store');

function listarObservacion(){
    return new Promise((resolve, reject)=>{
        resolve(store.listarObservacion());
    })
}
function agregarObservacion(observacion){
    return new Promise((resolve, reject)=>{
        if(!observacion.itemObservacion || !observacion.usuarioObservacion || !observacion.observacion){
            console.error('[Controller Observacion] validar que los campos no estan vacios');
            reject('Algun campo llego vacio');
            return false;
        }
        const fullObservacion = {
            item : observacion.itemObservacion,
            usuario : observacion.usuarioObservacion,
            observacion : observacion.observacion
        }
        resolve(store.agregarObservacion(fullObservacion));
    })
}
function actualizarObservacion(id, observacion){
    return new Promise((resolve, reject)=>{
        if(!id || !observacion.itemObservacion || !observacion.usuarioObservacion || !observacion.observacion){
            console.error('[Controller Observacion]');
            reject('');
            return false;
        }
        const fullObservacion = { 
            id, 
            item : observacion.itemObservacion,
            usuario : observacion.usuarioObservacion,
            observacion : observacion.observacion
        }
        resolve(store.actualizarObservacion(fullObservacion))
    })
}
function eliminarObservacion(id){
    return new Promise((resolve, reject)=>{
        if (!id) {
            console.error('[Controller Observacion] el identificador de la observacion es vacio');
            reject('Verificar campo llego vacio');
            return false;
        }
        resolve(store.eliminarObservacion(id));
    })
}

module.exports = {
    listarObservacion,
    agregarObservacion,
    actualizarObservacion,
    eliminarObservacion
}