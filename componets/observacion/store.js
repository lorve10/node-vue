const {conn} = require('../../settings/db')

function listarObservacion(){
    return new Promise ((resolve, reject)=>{
        
        conn.getConnection((error, connection)=>{
           
            if (error) { throw error; }
            connection.beginTransaction((error)=>{
                if (error) {throw error;}
                connection.query('SELECT id_observacion AS Uid, item_observacion AS item, usuario_observacion AS usuario, observacion_observacion AS observacion FROM observaciones',(error, result, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        resolve(result);
                    });
                })
            })
        })
    })
}

function agregarObservacion(observacion){
    return new Promise((resolve, reject)=>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){throw error;}
                connection.query('INSERT INTO observaciones (item_observacion, usuario_observacion, observacion_observacion) VALUES (?, ?, ?)', [observacion.item, observacion.usuario, observacion.observacion], (error, results, fields)=>{
                    connection.release();
                    if(error){
                        return connection.rollback(()=>{
                            throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){
                            return connection.rollback(()=>{
                                throw error;
                            })
                        }
                        connection.commit((error)=>{
                            if(error){
                                return connection.rollback(()=>{
                                    throw error;
                                })
                            }
                            resolve('exito');
                        })
                    });
                })
            })
        })
    })
}

function actualizarObservacion(observacion){
    return new Promise ((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if (error){throw error;}
            connection.beginTransaction((error)=>{
                if (error){throw error}
                connection.query('UPDATE observaciones SET item_observacion = ?, usuario_observacion = ?, observacion_observacion = ? WHERE id_observacion = ?', [observacion.item, observacion.usuario, observacion.observacion, formato.id], (error, results, fields)=>{
                    connection.release();
                    if (error) {
                        return connection.rollback(()=>{throw error;})
                    }

                    connection.commit((error)=>{
                        if (error) {
                            return connection.rollback(()=>{throw error;})
                        }
                        else if(results.affectedRows){
                            resolve('exito');
                        }
                        else {
                            console.error('No se pudo actualizar la observacion');
                            reject('Algho salio mal al actualizar la observacion');
                            return false;
                        }
                    });

                })
            })
        })
    })
}

function eliminarObservacion(id){
    return new Promise((resolve, reject) =>{
        conn.getConnection((error, connection)=>{
            if(error){
                throw error;
            }
            connection.beginTransaction((error)=>{
                if(error){ throw error; }
                connection.query('DELETE FROM observaciones WHERE id_observacion = ?', [id], (error, results, fields)=>{
                    connection.release();
                    if(error){ 
                        connection.rollback(()=>{ 
                        throw error;
                        })
                    }
                    connection.commit((error)=>{
                        if(error){connection.rollback(()=>{
                                throw error
                            })
                        }
                        else if(results.affectedRows){
                            resolve('exito')
                        }else{
                            console.error('No se pudo eliminar la observacion');
                            reject('Intenta nuevamente eliminar la observacion')
                            return false;
                        }
                    });
                })
            })
        })
    })
}

module.exports = {
    listarObservacion,
    agregarObservacion,
    actualizarObservacion,
    eliminarObservacion
}