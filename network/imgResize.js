const jimp = require('jimp');
const multer = require('multer');

const storage = multer.diskStorage({
    filename: function (req, file, cb) {
        // console.log(file);
        const mimetype = file.mimetype.split("/");
        var ext = mimetype[1];
        file.fieldname = Date.now() + Math.round(Math.random() * 1E9) + '.' + ext
        cb(null, file.fieldname)
    }
})

const upload = multer({
        storage,
        rename: function (fieldname, filename) {
            return filename + Date.now();
        },

        fileFilter: (req, file, cb) => {
            if (
                file.mimetype == "image/png" ||
                file.mimetype == "image/jpg" ||
                file.mimetype == "image/jpeg"
            ) {
                cb(null, true);
            } else {
                cb(null, false);
                return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
            }
        },

    })
    .fields([{
        name: "image"
    }, {
        name: "imageE"
    }]);
// .array('image', 5)

const resize = async (req, folder, next) => {
    if (req) {
      await req.forEach(async (element) => {
          const photo = await jimp.read(element.path);
          await photo.resize(500, jimp.AUTO);
          await photo.write(`./public/img/${folder}/${element.filename}`);
          await photo.resize(104, jimp.AUTO);
          await photo.write(`./public/img/${folder}/.Metadatos/${element.filename}`);
      });

    }
    else {
        next();
        return;
    }
    next();
};

const resize2 = async (req, folder, next) => {
    if (req) {
      const photo = await jimp.read(req.data);
        await photo.resize(500, jimp.AUTO);
        await photo.write(`./public/img/${folder}/${req.name}`);
        await photo.resize(104, jimp.AUTO);
        await photo.write(`./public/img/${folder}/.Metadatos/${req.name}`);
    }
    else {
        next();
        return;
    }
    next();
};
const resize3 = async (req, folder, next) => {
    if (req) {
      const photo = await jimp.read(req.data);
        await photo.resize(500, jimp.AUTO);
        await photo.write(`./public/${folder}/${req.name}`);
        await photo.resize(104, jimp.AUTO);
        await photo.write(`./public/${folder}/.Metadatos/${req.name}`);
    }
    else {
        next();
        return;
    }
    next();
};

module.exports = {
    upload,
    resize,
    resize2,
    resize3
}
