// Rutas.
const asignacion = require('../componets/asignacion/network'); // Componente personas.
const persona = require('../componets/persona/network'); // Componente personas.
const documento = require('../componets/documento/network'); // Componente personas.
const item = require('../componets/item/network'); // Componente item.
const inspeccion = require('../componets/inspeccion/network'); // Componente inspeccion.
const usuario = require('../componets/usuario/network'); // Componente usuarios.
const empresa = require('../componets/empresa/network'); // Componente empresa.
const roles = require('../componets/rol/network'); // Componente rol.
const permisos = require('../componets/permiso/network'); // Componente permiso.
const pais = require('../componets/pais/network'); // Componente pais.
const cargo = require('../componets/cargo/network'); // Componente cargo.
const tipo_tratamiento = require('../componets/tipo_tratamiento/network'); // Componente tratamiento.
const plantilla = require('../componets/plantilla/network'); // Componente tratamiento.
const fabricante = require('../componets/fabricante/network'); // Componente fabricante.
const estado_item = require('../componets/estado_item/network'); // Componente estado item.
const estado_remision = require('../componets/estado_remision/network'); // Componente estado item.
const uso_item = require('../componets/uso_item/network'); // Componente uso item.
const tipo_item = require('../componets/tipo_item/network'); // Componente tipo item.
const tipo_equipo = require('../componets/tipo_equipo/network'); // Componente tipo equipo.
const bodega = require('../componets/bodega/network'); // Componente bodega.
const marca = require('../componets/marca/network'); // Componente marca.
const formato = require('../componets/formato/network'); // Componente formato.
const departamento = require('../componets/departamento/network'); // Componente departamento.
const municipio = require('../componets/municipio/network'); // Componente municipio.
const tratamiento = require('../componets/tratamiento/network'); // Componente tratamiento.
const formato_criterio = require('../componets/formato_criterio/network'); // Componente formato criterio.
const sede = require('../componets/sede/network'); // Componente sede.
const observacion = require('../componets/observacion/network'); // Componente observacion.
const proveedor = require('../componets/proveedor/network'); // Componente proveedor.
const remision_imagen = require('../componets/remision_imagen/network'); // Componente remision_imagen.
const grupo = require('../componets/grupo/network'); // Componente grupo.
const formato_inspeccion = require('../componets/formato_inspeccion/network');// componente formulario inspeccion
const certificado = require('../componets/certificado_if098/network'); // componente de certificadoi certificado_if098

const routes = (server) => {
    server.use('/asignacion', asignacion);
    server.use('/bodega', bodega);
    server.use('/cargo', cargo);
    server.use('/departamento', departamento);
    server.use('/documento', documento);
    server.use('/empresa', empresa);
    server.use('/estado_item', estado_item);
    server.use('/estado_remision', estado_remision);
    server.use('/fabricante', fabricante);
    server.use('/formato', formato);
    server.use('/formato_criterio', formato_criterio);
    server.use('/grupo', grupo);
    server.use('/inspeccion', inspeccion);
    server.use('/item', item);
    server.use('/marca', marca);
    server.use('/municipio', municipio);
    server.use('/observacion', observacion);
    server.use('/pais', pais);
    server.use('/permiso', permisos);
    server.use('/persona', persona);
    server.use('/plantilla', plantilla);
    server.use('/proveedor', proveedor);
    server.use('/remision_imagen', remision_imagen);
    server.use('/rol', roles);
    server.use('/sede', sede);
    server.use('/tipo_equipo', tipo_equipo);
    server.use('/tipo_item', tipo_item);
    server.use('/tipo_tratamiento', tipo_tratamiento);
    server.use('/tratamiento', tratamiento);
    server.use('/uso_item', uso_item);
    server.use('/usuario', usuario);
    server.use('/formato_inspeccion', formato_inspeccion);
    server.use('/certificado', certificado);
}

// Exportamos las funciones (modulos).
module.exports = routes;
