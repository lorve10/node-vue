const {
    keys
} = require('../settings/key'); // Invocación de la llave secreta.
const jwt = require("jsonwebtoken"); // Invocación del sistema de Tokens.

// Verificación si es valido el Token.
const verifyToken = (req, res, next) => {
    let tokens = req.headers['auth-token'] || req.headers['x-access-token'] || req.headers['authorization']; // Llegada del Token por Headers.
    // Validación si no posee Token.
    if (!tokens) {
        res.status(401).send({
            error: 'tokenNull', // Mensaje de validación de error.
        });
        return;
    }
    // Validación para quitar palabra Bearer.


    if (tokens.startsWith('Bearer ')) {
        tokens = tokens.slice(7, tokens.length);
    }
    // Validación si hay un Token.
    if (tokens) {
        // Verificación de Token.
        jwt.verify(tokens, keys, (error, decoded) => {
            if (error) {
                return res.json({
                    error: '!token', // Mensaje de validación de error.
                });
            } else {
                req.decoded = decoded.user;
                // console.log(req.decoded);
                next(); // Da acceso si es valido el Token.
            }
        });
    }

}

// Verificación si es admin.
const sAdmin = (req, res, next) => {
    if ((req.decoded[0].permiso.indexOf(0) != -1)) {
        next();
        return;
    }
    return res.status(403).json({
        message: 'No tienes los permisos suficientes', // Mensaje de validación de error.
    });
}
const agregar = (req, res, next) => {
    if ((req.decoded[0].permiso.indexOf(1) != -1) || (req.decoded[0].permiso.indexOf(0) != -1) || (req.decoded[0].permiso.indexOf(4) != -1)) {
        next();
        return;
    }
    return res.status(403).json({
        message: 'No tienes los permisos suficientes', // Mensaje de validación de error.
    });
}

const editar = (req, res, next) => {
    if ((req.decoded[0].permiso.indexOf(2) != -1) || (req.decoded[0].permiso.indexOf(0) != -1) || (req.decoded[0].permiso.indexOf(4) != -1)) {
        next();
        return;
    }
    return res.status(403).json({
        message: 'No tienes los permisos suficientes', // Mensaje de validación de error.
    });

}

const eliminar = (req, res, next) => {
    if ((req.decoded[0].permiso.indexOf(3) != -1) || (req.decoded[0].permiso.indexOf(0) != -1)) {
        next();
        return;
    }
    return res.status(403).json({
        message: 'No tienes los permisos suficientes', // Mensaje de validación de error.
    });

}

module.exports = {
    verifyToken,
    sAdmin,
    agregar,
    editar,
    eliminar
}
